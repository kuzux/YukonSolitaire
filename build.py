#!/usr/bin/env python3

import sys
import os
import platform
import shutil
import glob
import subprocess
import argparse
import time
import json
# TODO: What if the python version is old enough that this doesn't exist?
from concurrent.futures import ThreadPoolExecutor

default_debugger = shutil.which("lldbui")
if default_debugger is None:
    default_debugger = shutil.which("lldb")
if default_debugger is None:
    default_debugger = shutil.which("gdb")

parser = argparse.ArgumentParser()
parser.add_argument('--time', action="store_true", help="Print the time taken for the selected action")
parser.add_argument('--release', action="store_true", help="Build in release configuration")
parser.add_argument('-j', '--jobs', type=int, help="Number of jobs to run in parallel", action="store", default=os.cpu_count() or 1)

subcommands = parser.add_subparsers(dest="subcommand", required=True)

subcommands.add_parser("preprocess", help="Convert images, generate code etc.")
subcommands.add_parser("vscode-setup", help="Generate .vscode/c_cpp_properties.json")
subcommands.add_parser("hooks-setup", help="Setup git hooks")
subcommands.add_parser("clean", help="Remove build artifacts")
subcommands.add_parser("check-format", help="Check if c++ source files are formatted correctly (Used by the git hook)")
subcommands.add_parser("format", help="Auto-format the c++ files")

build_parser = subcommands.add_parser("build", help="Build the project (default action)")
build_parser.add_argument('--rebuild', action="store_true")

run_parser = subcommands.add_parser("run", help="Build and run the project")
run_parser.add_argument('extra', nargs=argparse.REMAINDER)
run_parser.add_argument('--debug', nargs="?", const=default_debugger, required=False, help="Run the program in a debugger")

count_lines = subcommands.add_parser("count-lines", help="Count the number of lines of code via cloc")
create_dmg = subcommands.add_parser("create-dmg", help="Create a .dmg file for macOS")
create_appimage = subcommands.add_parser("create-appimage", help="Create an AppImage for Linux")

args_to_parse = ["build"] if len(sys.argv) == 1 else sys.argv[1:]
default_args = argparse.Namespace(rebuild=False, time=False, release=False, extra=[])
args = parser.parse_args(args_to_parse, default_args)

clang_format = shutil.which("clang-format")
cxx = shutil.which("g++")
cxx_flags = "-fsanitize=address -Wall -Wextra -Wunused-variable -std=c++20".split()
ld_flags = "-fsanitize=address -lm".split()
executable = "dest/yukon"

linuxdeploy = os.path.expanduser("~/opt/linuxdeploy-x86_64.AppImage")
appimagetool = os.path.expanduser("~/opt/appimagetool-x86_64.AppImage")

if args.release:
    cxx_flags += ["-O3"]
    ld_flags += ["-O3"]
else:
    cxx_flags += ["-g"]
    ld_flags += ["-g"]

pkgs = ["fmt", "sdl2", "glm", "glew", "sqlite3", "opusfile"]

if platform.system() == "Darwin":
    ld_flags += ['-framework', 'OpenGL', '-framework', 'AppKit']
else:
    pkgs += ["opengl"]

# type is one of "cflags" or "libs"
def pkg_config(type, pkgs):
    output = subprocess.check_output(["pkg-config", "--" + type, *pkgs])
    return output.decode('utf-8').split()

cxx_flags += pkg_config("cflags", pkgs)
ld_flags += pkg_config("libs", pkgs)

def run_cmd(args, output=True, exec=False):
    print("+ ", " ".join(args))
    stdout = None
    stderr = None
    if not output:
        stdout = subprocess.DEVNULL
        stderr = subprocess.DEVNULL
    
    result = None
    if exec:
        os.execvp(args[0], args)
    else:
        result = subprocess.run(args, stdout=stdout, stderr=stderr)

    if result.returncode != 0:
        cmd_string = " ".join(args)

        print("\x1b[31mCommand \x1b[1;31m", f"\"{cmd_string}\"", f"\x1b[0;31m failed with exit code {result.returncode}", "\x1b[0m")
        raise Exception("Command failed")

def object_file_path(src_path):
    first, rest = src_path.split(os.path.sep, 1)
    if first == "src":
        first = "dest"
    nonext, ext = rest.rsplit(".", 1)
    ext = "o"
    return os.path.join(first, f"{nonext}.{ext}")

def newest_modification_time(paths):
    def modtime(path):
        return os.path.getmtime(path)

    try:
        modtimes = list(map(modtime, paths))
        return max(modtimes)
    except OSError:
        return 0

def parse_makefile_deps(output_as_bytes):
    out = output_as_bytes.decode('utf-8').replace("\\\n", "").split(": ", 2)
    return out[1].split()

def cpp_source_dependencies(src):
    # TODO: Cache these dependencies as well
    return parse_makefile_deps(subprocess.check_output([cxx, "-M", src, *cxx_flags]))

def should_build(srcs, dests):
    src_time = newest_modification_time(srcs)
    dest_time = newest_modification_time(dests)

    return src_time > dest_time

def should_compile_cpp(src, dest):
    deps = cpp_source_dependencies(src)
    return should_build([src, *deps], [dest])

def compile_cpp(srcs):
    def compile_cpp_file(src):
        dest = object_file_path(src)
        if should_compile_cpp(src, dest):
            os.makedirs(os.path.dirname(dest), exist_ok = True)
            run_cmd([cxx, "-c", src, "-o", dest, *cxx_flags])

        return dest

    with ThreadPoolExecutor(max_workers=args.jobs) as executor:
        return list(executor.map(compile_cpp_file, srcs))

def compile_objc(srcs):
    objs = []
    for src in srcs:
        dest = object_file_path(src)
        if should_build([src], [dest]):
            os.makedirs(os.path.dirname(dest), exist_ok = True)
            run_cmd(["clang", "-x", "objective-c", "-c", src, "-o", dest])

        objs.append(dest)
    return objs

def compile_c(srcs):
    objs = []
    for src in srcs:
        dest = object_file_path(src)
        if should_build([src], [dest]):
            os.makedirs(os.path.dirname(dest), exist_ok = True)
            run_cmd(["gcc", "-Wall", "-Wextra", "-c", src, "-o", dest])

        objs.append(dest)
    return objs

def link(objs, dest):
    src_time = newest_modification_time(objs)
    dest_time = newest_modification_time([dest])

    if src_time > dest_time:
        run_cmd([cxx, "-o", dest, *objs, *ld_flags])

# Subcommands

def vscode_setup():
    os_triplets = { "Darwin": "macos", "Linux": "linux" }
    arch_triplets = { "x86_64": "x64", "arm64": "arm64" }

    intellisense_mode = f"{os_triplets[platform.system()]}-gcc-{arch_triplets[platform.machine()]}"

    config = {
        "configurations": [
            {
                "name": "YukonSolitaire",
                "includePath": [
                    "${workspaceFolder}/**" # To be filled later
                ],
                "defines": [], # To be filled later
                "compilerPath": cxx,
                "cStandard": "c17",
                "cppStandard": "c++20",
                "intelliSenseMode": intellisense_mode
            }
        ],
        "version": 4
    }
    for flag in cxx_flags:
        if flag.startswith("-I"):
            config["configurations"][0]["includePath"].append(flag[2:])
        elif flag.startswith("-D"):
            config["configurations"][0]["defines"].append(flag[2:])
    
    os.makedirs(".vscode", exist_ok = True)
    with open(".vscode/c_cpp_properties.json", "w") as f:
        json.dump(config, f, indent=4)

    if platform.system() == "Darwin":
        debugger_mode = "lldb"
        config_type = "lldb"
    else:
        debugger_mode = "gdb"
        config_type = "cppdbg"
    
    launch = {
        "version": "0.2.0",
        "configurations": [
            {
                "name": "Attach to process",
                "type": config_type,
                "MIDebuggerMode": debugger_mode,

                "request": "attach",
                "processId": "${command:pickProcess}",
                "program": f"${{workspaceFolder}}/{executable}",
            },
            {
                "name": "Run in debugger",
                "type": config_type,
                "MIDebuggerMode": debugger_mode,

                "request": "launch",
                "program": f"${{workspaceFolder}}/{executable}",
                "args": [],
                "environment": [],
                "cwd": "${workspaceFolder}",
            }
        ]
    }
    with open(".vscode/launch.json", "w") as f:
        json.dump(launch, f, indent=4)

def hooks_setup():
    os.makedirs(".git/hooks", exist_ok = True)
    if os.path.exists(".git/hooks/pre-commit"):
        raise Exception("pre-commit hook already exists")
    with open(".git/hooks/pre-commit", "w") as f:
        f.write("#!/bin/sh\n")
        f.write("python3 build.py check-format\n")
    os.chmod(".git/hooks/pre-commit", 0o755)

def clean():
    shutil.rmtree("dest")

def build():
    if args.rebuild:
        clean()
    preprocess()
    objs = []
    objs += compile_cpp(glob.glob("src/**/*.cpp", recursive=True))
    if platform.system() == "Darwin":
        objs += compile_objc(glob.glob("src/platform/*_osx.m"))
    elif platform.system() == "Linux":
        objs += compile_c(glob.glob("src/platform/*_linux.c"))
    link(objs, executable)

def format(check_only=False):
    files_to_format = glob.glob("src/**/*.cpp", recursive=True)
    files_to_format += glob.glob("src/**/*.h", recursive=True)
    files_to_format = [f for f in files_to_format if not f.startswith("src/vendor")]

    if check_only:
        # TODO: Only check changes in the git index
        try:
            run_cmd(["clang-format", "-i", "--dry-run", "--Werror", *files_to_format], output=False)
        except:
            print("Formatting check failed, run `./build.py --format` to auto-format the files", file = sys.stderr)
            raise SystemExit(1)
    else:
        run_cmd(["clang-format", "-i", *files_to_format])

def run():
    build()
    extra_args = args.extra[1:]
    if args.debug is not None:
        debugger = args.debug
        print(f"Running in debugger {debugger}")
        run_cmd([debugger, executable, "--", *extra_args], exec=True)
    else:
        run_cmd([executable, *extra_args])


def preprocess():
    srcs = ["assets/sheet.xml", "assets/icon.svg"]
    dests = ["dest/SpriteSheet.h", "dest/icon.png"]
    if not should_build(srcs, dests):
        return
    with ThreadPoolExecutor(max_workers=args.jobs) as executor:
        executor.submit(run_cmd, ['python3', './assets/create_sheet_class.py', '--name', 'SpriteSheet', '--src', 'assets/sheet.xml', '--dest', 'dest/SpriteSheet.h'])
        executor.submit(run_cmd, ['magick', '-density', '1200', '-background', 'none', 'assets/icon.svg', '-gravity', 'center', '-resize', '256x256', '-extent', '256x256',  'dest/icon.png'])
        executor.submit(run_cmd, ['magick', '-density', '1200', '-background', 'none', 'assets/icon.svg', '-resize', '27x32', 'dest/icon_sm.png'])


def create_dmg():
    build()
    dest_dir = "dest/YukonSolitaire.app"
    shutil.rmtree(dest_dir, ignore_errors=True)

    os.makedirs(f"{dest_dir}/Contents/Frameworks", exist_ok = True)
    os.makedirs(f"{dest_dir}/Contents/Library", exist_ok = True)
    os.makedirs(f"{dest_dir}/Contents/MacOS", exist_ok = True)
    os.makedirs(f"{dest_dir}/Contents/PlugIns", exist_ok = True)
    os.makedirs("dest/yukon.iconset", exist_ok = True)
    aspect_ratio = 214/256
    def create_icon(height, name):
        width = int(height * aspect_ratio)
        src_file = "assets/icon.svg"
        dest_file = f"dest/yukon.iconset/icon_{name}.png"
        if not should_build([src_file], [dest_file]):
            return
        resize_str = f"{height}x{height}"
        run_cmd(['magick', '-density', '1200', '-background', 'none', src_file, '-gravity', 'center', '-resize', resize_str, '-extent', resize_str, dest_file])

    create_icon(16, "16x16")
    create_icon(32, "16x16@2x")
    create_icon(32, "32x32")
    create_icon(64, "32x32@2x")
    create_icon(128, "128x128")
    create_icon(256, "128x128@2x")
    create_icon(256, "256x256")
    create_icon(512, "256x256@2x")
    create_icon(512, "512x512")
    create_icon(1024, "512x512@2x")

    executable_in_app_dir = f"{dest_dir}/Contents/MacOS/{executable.split('/')[-1]}"

    run_cmd(["iconutil", "-c", "icns", "dest/yukon.iconset"])
    shutil.copy("assets/Info.plist", f"{dest_dir}/Contents/Info.plist")
    shutil.copy(executable, f"{dest_dir}/Contents/MacOS/")
    shutil.copytree("assets", f"{dest_dir}/Contents/Resources/", dirs_exist_ok=True)
    shutil.move("dest/yukon.icns", f"{dest_dir}/Contents/Resources/yukon.icns")
    shutil.copy("dest/icon.png", f"{dest_dir}/Contents/Resources/icon.png")
    
    def find_deps(base_path):
        lines = subprocess.check_output(f'otool -L {base_path}', shell=True).decode('utf-8').split("\n")
        all_deps = [dep.split()[0].strip() for dep in lines[1:-1]]
        brew_deps = [dep for dep in all_deps if "homebrew" in dep and dep != base_path]
        return brew_deps
    brew_deps = find_deps(executable_in_app_dir)
    for dep in brew_deps:
        run_cmd(["install_name_tool", "-change", dep, f"@executable_path/../Library/{dep.split('/')[-1]}", executable_in_app_dir])
        path_in_app_dir = f"{dest_dir}/Contents/Library/{dep.split('/')[-1]}"
        shutil.copy2(dep, path_in_app_dir)
        os.chmod(path_in_app_dir, 0o755)
        transitive_deps = find_deps(dep)
        for transitive_dep in transitive_deps:
            # TODO: This only goes one level deep, we should go deeper (use a recursive function)
            run_cmd(["install_name_tool", "-change", transitive_dep, f"@executable_path/../Library/{transitive_dep.split('/')[-1]}", path_in_app_dir])
            target_path = f"{dest_dir}/Contents/Library/{transitive_dep.split('/')[-1]}"
            shutil.copy2(transitive_dep, target_path)

    run_cmd(["codesign", "--force", "--deep", "--sign", "-", dest_dir])
    os.chdir("dest")
    run_cmd(["dmgbuild", "-s", "../assets/dmgbuild.json", "YukonSolitaire.app", "YukonSolitaire.dmg"])


def create_appimage():
    build()
    dest_dir = "dest/YukonSolitaire.AppDir"
    shutil.rmtree(dest_dir, ignore_errors=True)
    run_cmd([linuxdeploy, "-e", executable, f"--appdir={dest_dir}", "-i", "assets/icon.svg", "-d", "assets/YukonSolitaire.desktop"])
    shutil.copytree("assets", f"{dest_dir}/usr/share/", dirs_exist_ok=True)
    shutil.copy("dest/icon.png", f"{dest_dir}/usr/share/icon.png")
    run_cmd([appimagetool, dest_dir, "dest/YukonSolitaire.AppImage"])


start_time = time.time()

subcommands = {
    "preprocess": preprocess,
    "vscode-setup": vscode_setup,
    "hooks-setup": hooks_setup,
    "clean": clean,
    "check-format": format,
    "format": format,
    "build": build,
    "run": run,
    "count-lines": lambda: run_cmd(["cloc", "src/", "--exclude-dir=vendor"]),
    "create-dmg": create_dmg,
    "create-appimage": create_appimage
}

if args.subcommand not in subcommands:
    print(f"Unknown subcommand {args.subcommand}", file = sys.stderr)
    raise SystemExit(1)

subcommands[args.subcommand]()

end_time = time.time()

if args.time:
    print(f"Build took {end_time - start_time} seconds")
