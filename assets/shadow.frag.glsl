#version 330 core

out vec4 color;
in vec2 card_uv;

void main()
{
    // https://discourse.threejs.org/t/give-a-border-blur-effect-with-glsl/47863/2
    float edge = .04f;
    float edge_min = edge;
    float edge_max = 1.0 - edge;

    float xp = smoothstep(0.0, edge_min, card_uv.x);
    float xn = smoothstep(1.0, edge_max, card_uv.x);
    float yp = smoothstep(0.0, edge_min, card_uv.y);
    float yn = smoothstep(1.0, edge_max, card_uv.y);

    float alpha = (xp*xn*yp*yn) * 0.7;
    color = vec4(0.0, 0.0, 0.0, alpha);
}
