#version 330 core

layout (location = 0) in vec2 vertex_pos;
layout (location = 1) in vec2 uv_in;

layout (location = 2) in vec2 position;
layout (location = 3) in vec2 scale;
layout (location = 4) in float z_index;

layout (location = 5) in vec2 uv_position;
layout (location = 6) in vec2 uv_scale;

layout (location = 7) in float highlighted_in;

out vec2 card_uv;
out vec2 spritesheet_uv;

void main()
{
    vec2 shadow_offset = vec2(0.01, -0.01);
    gl_Position = vec4((vertex_pos * scale) + position + shadow_offset, z_index + 0.001, 1.0);
    card_uv = uv_in;
    spritesheet_uv = uv_in * uv_scale + uv_position;
}
