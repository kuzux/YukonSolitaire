#version 330 core

out vec4 color;
in float z_index;

void main()
{
    if (z_index < 0) {
        color = vec4(1.0, 0.0, 0.0, 1.0);
    } else {
        color = vec4(0.0, 0.0, 1.0, 1.0);
    }
}
