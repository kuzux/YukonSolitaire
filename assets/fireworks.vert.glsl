#version 330 core

layout (location = 0) in vec2 pos_in;
layout (location = 1) in vec2 uv_in;

out vec2 uv_vert;

void main()
{
    gl_Position = vec4(pos_in, -0.99, 1.0);
    uv_vert = uv_in;
}
