#version 330 core

layout (location = 0) in vec2 vertex_pos;
layout (location = 1) in vec2 uv_in;

layout (location = 2) in vec2 position;
layout (location = 3) in vec2 scale;
layout (location = 4) in float z_index_in;

layout (location = 5) in vec2 uv_position;
layout (location = 6) in vec2 uv_scale;

layout (location = 7) in float highlighted_in;

out vec2 card_uv;
out vec2 spritesheet_uv;
out float highlighted;

out float z_index;

void main()
{
    gl_Position = vec4((vertex_pos * scale) + position, z_index_in, 1.0);
    z_index = z_index_in;
    card_uv = uv_in;
    highlighted = highlighted_in;
    spritesheet_uv = uv_in * uv_scale + uv_position;
}
