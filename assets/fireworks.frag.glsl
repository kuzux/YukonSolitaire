#version 330 core

out vec4 frag_color;
in vec2 uv_vert;

uniform float aspect_ratio;
uniform float time;

#define NUM_PARTICLES 100.
#define EXPLOSION_TIME 1.5

// t shouldn't be equal to 0. If it does, the function always returns (0, 0)
// returns a random vec2 of length <1
vec2 hash12p(float t)
{
    float r = fract(sin(t*674.3)*453.2);
    float th = 6.28328*fract(sin((t+r)*714.3)*263.2);
    return r*vec2(cos(th), sin(th));
}

// returns (x, y, z) where x, y, z are in [0, 1)
vec3 hash13(float t)
{
    float x = fract(sin(t*674.3)*453.2);
    float y = fract(sin((t+x)*714.3)*263.2);
    float z = fract(sin((t+y)*357.9)*123.4);

    return vec3(x, y, z);
}

// returns (x, y, z) where x, y, z are in [0, 1)
vec2 hash12(float t)
{
    float x = fract(sin(t*674.3)*453.2);
    float y = fract(sin((t+x)*714.3)*263.2);

    return vec2(x, y);
}

float hash11(float t)
{
    float x = fract(sin(t*674.3)*453.2);

    return x;
}

float explosion(float n, vec2 uv, float t)
{
    float explosion_size = hash11(n+1.)*.3+.2;

    float sparks = 0.;
    for(float i = 0.; i < NUM_PARTICLES; i++)
    {
        vec2 dir = hash12p(i+1.)*explosion_size;

        float d = length(uv-dir*t);

        float brightness = mix(0.001, 0.003, smoothstep(.05, 0., t));
        
        // sparkle of the fireworks particles
        brightness *= sin(t*20.+i)*.35+.65;

        brightness *= smoothstep(1., .5, t);
        
        sparks += brightness/d;
    }
    return sparks;
}

void main()
{
    // following this tutorial https://www.youtube.com/watch?v=xDxAnguEOn8
    //     but not quite the same
    vec2 aspect_ratio_correction = vec2(aspect_ratio, 1.);

    vec2 uv = (uv_vert-.5)*aspect_ratio_correction;
    float t = fract(time/EXPLOSION_TIME);
    float n = floor(time/EXPLOSION_TIME);

    vec3 brightness = vec3(0.0);
    brightness += explosion(n, uv-hash12(n)*aspect_ratio_correction*0.4, t);

    vec3 color = hash13(n)*.5+.5;
    float alpha = brightness.r;

    frag_color = vec4(color*brightness, alpha);
}
