import argparse
from xml.dom.minidom import parse
import sys
import os

parser = argparse.ArgumentParser()
parser.add_argument('--name', type=str, help='The name of the class to create')
parser.add_argument('--src', type=str, help='The source XML file')
parser.add_argument('--dest', type=str, help='The destination C++ file')
args = parser.parse_args()

with open(args.src) as f:
    doc = parse(f)

sheet_width = int(doc.getElementsByTagName('sheet')[0].getAttribute('width'))
sheet_height = int(doc.getElementsByTagName('sheet')[0].getAttribute('height'))

os.makedirs(os.path.dirname(args.dest), exist_ok=True)
with open(args.dest, 'w') as f:
    indentation = 0
    def write_line(line):
        f.write(" " * indentation + line + "\n")
    def indent():
        global indentation
        indentation += 4
    def dedent():
        global indentation
        indentation -= 4
        if indentation < 0:
            indentation = 0
    cmdline = " ".join(sys.argv)
    write_line(f"// Created via {cmdline}")
    write_line("#pragma once")
    write_line("")
    write_line("#include <unordered_map>")
    write_line("#include <string>")
    write_line("#include <glm/glm.hpp>")
    # TODO: Actually resolve the import
    write_line("#include \"../src/Error.h\"")
    write_line("")
    write_line(f"class {args.name} {{"); indent()
    write_line("public:")
    write_line("struct SubTexture {"); indent()
    write_line("glm::vec2 position;")
    write_line("glm::vec2 size;"); dedent()
    write_line("};")
    # TODO: Add a way to tell what png file to read
    write_line("ErrorOr<SubTexture> get_subtexture(std::string const& name) const {"); indent()
    write_line("if (subtextures.find(name) == subtextures.end()) return Error::with_message(std::string(\"Subtexture not found\"));")
    write_line("return subtextures.find(name)->second;"); dedent()
    write_line("};")
    write_line("")
    write_line("private:")
    write_line("std::unordered_map<std::string, SubTexture> subtextures = {"); indent()
    for node in doc.getElementsByTagName('SubTexture'):
        name = node.getAttribute('name')
        x = int(node.getAttribute('x'))
        y = int(node.getAttribute('y'))
        width = int(node.getAttribute('width'))
        height = int(node.getAttribute('height'))

        position = f"{x / sheet_width}, {y / sheet_height}"
        size = f"{width / sheet_width}, {height / sheet_height}"
        write_line(f"{{ \"{name}\", {{ glm::vec2({position}), glm::vec2({size}) }} }},")
    dedent()
    write_line("};"); dedent()
    write_line("};")
