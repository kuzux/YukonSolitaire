#version 330 core

out vec4 color;
in vec2 card_uv;

void main()
{
    color = vec4(card_uv, 0.0, 1.0);
}
