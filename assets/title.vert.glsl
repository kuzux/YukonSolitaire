#version 330 core

layout (location = 0) in vec2 vertex_pos;
layout (location = 1) in vec2 uv_in;

uniform vec2 position;
uniform vec2 scale;

out vec2 uv;

void main()
{
    gl_Position = vec4((vertex_pos * scale) + position, 0.0, 1.0);
    uv = uv_in;
}
