from xml.dom.minidom import parse, getDOMImplementation

# The sprite sheeets are from https://opengameart.org/content/boardgame-pack, Kenney.nl

# Unified the two sprite sheets with this command:
# montage playingCardBacks.png playingCards.png -tile 2x1 -geometry +0+0 -background none sheet.png
# playingCardBacks.png was 512x2048, so I offset the faces by 512 pixels in the x direction

impl = getDOMImplementation()
outdoc = impl.createDocument(None, 'sheet', None)
root = outdoc.documentElement

with open('playingCardBacks.xml') as f:
    backs = parse(f)

with open('playingCards.xml') as f:
    faces = parse(f)

root.attributes["imagePath"] = "sheet.png"
root.attributes["width"] = "1536"
root.attributes["height"] = "2048"

for back in backs.getElementsByTagName('SubTexture'):
    root.appendChild(back)

for face in faces.getElementsByTagName('SubTexture'):
    face.attributes["x"] = str(int(face.attributes["x"].value) + 512)
    root.appendChild(face)

with open('sheet.xml', 'w') as f:
    f.write(root.toprettyxml(indent="    "))
