#version 330 core

out vec4 color;
in vec2 card_uv;
in vec2 spritesheet_uv;
in float highlighted;

uniform sampler2D spritesheet;

// from https://stackoverflow.com/a/30545544
float distance_to_rect(vec2 uv, vec2 top_left, vec2 bottom_right)
{
    vec2 d = max(top_left-uv, uv-bottom_right);
    return length(max(vec2(0.0), d)) + min(0.0, max(d.x, d.y));
}

void main()
{
    vec4 texture_color = texture(spritesheet, spritesheet_uv);
    float highlight_intensity = distance_to_rect(card_uv, vec2(0.2, 0.2), vec2(0.8, 0.8));
    vec4 highlight_color = vec4(.8, .8, .3, 1.0);
    color = mix(texture_color, highlight_color, highlighted * highlight_intensity);
}
