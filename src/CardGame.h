#pragma once

#include <glm/glm.hpp>
#include <memory>
#include <vector>

#include "AbstractPile.h"
#include "Card.h"
#include "Emitter.h"
#include "FoundationPile.h"
#include "GroupOfCards.h"
#include "Pile.h"
#include "UndoStack.h"

class GameEvent
{
public:
    enum class Type { Started, CardMoved, GameWon };

    GameEvent(Type type)
        : m_type(type)
    {
    }
    Type type() const { return m_type; }

private:
    Type m_type;
};

class CardGame : public Emitter<GameEvent>
{
public:
    static CardGame& the() { return *s_the; }

    CardGame();
    ~CardGame();
    CardGame& operator=(CardGame const&) = delete;
    CardGame(CardGame const&) = delete;

    void tick_time(float delta_seconds);
    uint64_t game_seed() const { return m_game_seed; }

    void mouse_down(glm::vec2 const& position);
    void mouse_moved_to(glm::vec2 const& position);
    void mouse_up(glm::vec2 const& position);

    glm::vec2 card_scale() const { return m_card_scale; }
    void card_scale(glm::vec2 const& scale) { m_card_scale = scale; }

    bool can_auto_win() const;
    void auto_win();

    bool game_won() const { return m_game_won; }
    float game_time() const { return m_game_time; }
    float time_after_winning() const { return m_time_after_winning; }

    void record_current_game();
    void reset_game();
    void new_game();

    std::vector<AbstractPile*> const& all_piles() const { return m_all_piles; }
    std::vector<Card>& deck() { return m_deck; }

    bool can_undo() const { return m_undo_stack.can_undo(); }
    void undo();

    bool can_redo() const { return m_undo_stack.can_redo(); }
    void redo();

private:
    inline static CardGame* s_the { nullptr };

    void mouse_drag_end(glm::vec2 const& position);
    void mouse_clicked(glm::vec2 const& position);

    Card* find_card_at(glm::vec2 const& position);
    AbstractPile* find_empty_pile_at(glm::vec2 const& position);
    AbstractPile* find_pile_for_cards(GroupOfCards const& card) const;
    bool check_game_won() const;
    void mark_as_won();

    void start_game();

    std::vector<Card> m_deck;
    std::vector<Pile> m_piles;
    std::vector<FoundationPile> m_foundation_piles;
    std::vector<AbstractPile*> m_all_piles;

    uint64_t m_game_seed { 0 };
    size_t m_number_of_moves { 0 };

    UndoStack m_undo_stack;

    Card* m_highlighted_card { nullptr };
    // TODO: This could be std::optional but couldn't get it to work with copy/move
    //       constructors etc.
    std::unique_ptr<GroupOfCards> m_dragged_cards { nullptr };
    glm::vec2 m_drag_start_position;
    glm::vec2 m_current_drag_position;

    glm::vec2 m_card_scale { 1.0f, 1.0f };

    float m_game_time { 0.0f };

    // Should be initialized to a random integer value to get a "random" winning animation
    float m_time_after_winning { 0.0f };
    bool m_game_won { false };
};