#pragma once

#include <functional>
#include <queue>
#include <string>
#include <string_view>
#include <unordered_map>

#include "AudioPlayer.h"
#include "Emitter.h"
#include "Error.h"

#include <SDL.h>
#include <glm/vec2.hpp>

// clang-format off
#define USE_IMGUI
#define IMGUI_HEADER(h) vendor/h
// clang-format on

class SDLEventLoop;
class SDLWindow
{
    friend class SDLEventLoop;

public:
    // The first sdl window created will be the primary window
    SDLWindow(std::string_view title, glm::ivec2 size);
    ~SDLWindow();

    SDLWindow& operator=(SDLWindow const&) = delete;
    SDLWindow(SDLWindow const&) = delete;

    static SDLWindow& the() { return *s_the; }
    static SDLWindow& get(uint32_t id) { return *s_windows[id]; }

    void draw();
    uint32_t id() const { return SDL_GetWindowID(window); }
    glm::ivec2 window_size() const { return m_window_size; }
    float aspect_ratio() const { return (float)m_window_size.x / (float)m_window_size.y; }

    void set_window_size(glm::ivec2 size) { m_window_size = size; }
    void set_title(std::string_view title);
    void set_icon(std::string const& filename);
    ErrorOr<void> setup_imgui();
    AudioPlayer& audio_player();

    void close() { SDL_DestroyWindow(window); }
    void make_current() { SDL_GL_MakeCurrent(window, ctx); }

    glm::vec2 gl_coordinates(glm::ivec2 sdl_coordinates) const;

    bool fullscreen() const { return SDL_GetWindowFlags(window) & SDL_WINDOW_FULLSCREEN; }
    void fullscreen(bool value);

private:
    inline static SDLWindow* s_the { nullptr };
    inline static std::unordered_map<uint32_t, SDLWindow*> s_windows;

    SDL_Window* window = nullptr;
    SDL_GLContext ctx;

    glm::ivec2 m_window_size;
    SDL_Surface* m_window_icon { nullptr };
    AudioPlayer* m_audio_player { nullptr };

    bool m_vsynced { false };
    bool m_using_imgui { false };
    bool m_primary { false };
};

class Timeout
{
public:
    enum class Result {
        Continue,
        Stop,
    };
    using Callback = std::function<Result(uint64_t)>;

    Timeout(uint64_t interval, Callback callback);
    ~Timeout();

    bool should_call(uint64_t ticks) const;
    Result call(uint64_t ticks);

private:
    uint64_t m_interval;
    uint64_t m_last_call;
    Callback m_callback;
};

class SDLEvent
{
public:
    using Type = SDL_EventType;
    SDLEvent(SDL_Event&& event)
        : m_event(event)
    {
    }

    SDL_EventType type() const { return (SDL_EventType)m_event.type; }

    SDL_Event const& event() const { return m_event; }
    SDL_WindowEvent const& window() const { return m_event.window; }
    SDL_KeyboardEvent const& key() const { return m_event.key; }
    SDL_MouseButtonEvent const& mouse_button() const { return m_event.button; }
    SDL_MouseMotionEvent const& mouse_motion() const { return m_event.motion; }
    SDL_KeyboardEvent const& key_event() const { return m_event.key; }

private:
    SDL_Event m_event;
};

class SDLFrameInfo
{
    friend class SDLEventLoop;

public:
    size_t frame_no() const { return m_frame_no; }

    uint64_t ticks() const { return m_ticks; }
    uint64_t delta_ticks() const { return m_delta_ticks; }

    float seconds() const { return m_seconds; }
    float delta_seconds() const { return m_delta_seconds; }

private:
    SDLFrameInfo(size_t frame_no, uint64_t ticks, uint64_t delta_ticks)
        : m_frame_no(frame_no)
        , m_ticks(ticks)
        , m_delta_ticks(delta_ticks)
    {
        m_seconds = (float)ticks / 1000.0f;
        m_delta_seconds = (float)delta_ticks / 1000.0f;
    }

    size_t m_frame_no;
    uint64_t m_ticks;
    uint64_t m_delta_ticks;
    float m_seconds;
    float m_delta_seconds;
};

class SDLEventLoop : public Emitter<SDLEvent>
{
    friend class SDLWindow;

public:
    static SDLEventLoop& the() { return *s_the; }
    static bool running() { return s_the && !s_the->stopped(); }
    SDLEventLoop();
    ~SDLEventLoop();

    void run(std::function<void(SDLFrameInfo const&)> body);

    void in(uint64_t interval, Timeout::Callback callback);
    void stop();
    bool stopped() const { return !m_running; }
    // TODO: Implement a way to get statistics about the event loop
private:
    bool m_running { false };
    std::queue<Timeout> m_timeouts;

    inline static SDLEventLoop* s_the { nullptr };
};
