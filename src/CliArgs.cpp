#include "CliArgs.h"

#include <fmt/core.h>

void CliArgument::add_alias(std::string const& alias)
{
    assert(alias.length() >= 2);
    assert(alias[0] == '-');
    if (alias.size() == 2) {
        m_short_aliases.push_back(alias[1]);
        return;
    }
    assert(alias[1] == '-');
    // TODO: Make sure the rest of the string is lowercase
    m_long_aliases.push_back(alias);
}

void CliArgument::print_usage()
{
    std::string text;
    std::vector<std::string> all_aliases;
    for (auto alias : m_short_aliases)
        all_aliases.push_back(fmt::format("-{}", alias));
    for (auto alias : m_long_aliases)
        all_aliases.push_back(alias);

    for (size_t i = 0; i < all_aliases.size(); i++) {
        text += all_aliases[i];
        if (i < all_aliases.size() - 1)
            text += ", ";
    }

    fmt::print(" {:<30} {}\n", text, m_description);
}

CliArgument& CliArgs::add_argument()
{
    m_arguments.emplace_back(false);
    return m_arguments.back();
}

CliArgument& CliArgs::add_flag()
{
    m_arguments.emplace_back(true);
    return m_arguments.back();
}

ErrorOr<void> CliArgs::parse()
{
    m_program_name = m_argv[0];

    // TODO: Keep the parser state in instance variables
    //       So we can use separate helper methods
    for (int i = 1; i < m_argc; i++) {
        auto& arg = m_argv[i];
        if (arg[0] != '-' || arg[1] == '\0')
            return Error::with_message("Invalid argument: {}", arg);
        if (!strcmp(arg, "-h") || !strcmp(arg, "--help"))
            return Error::with_message("Help requested");

        if (arg[1] != '-') {
            // Group of short arguments
            for (size_t j = 1; arg[j] != '\0'; j++) {
                char short_arg = arg[j];
                for (auto& argument : m_arguments) {
                    if (argument.m_short_aliases.empty())
                        continue;

                    for (auto alias : argument.m_short_aliases) {
                        if (alias != short_arg)
                            continue;
                        if (argument.m_is_flag) {
                            argument.m_raw_arg = (char*)0x1; // Some dummy value
                            break;
                        }
                        argument.m_raw_arg = m_argv[++i];
                        break;
                    }
                }
            }
        } else {
            // Long argument
            for (auto& argument : m_arguments) {
                if (argument.m_long_aliases.empty())
                    continue;

                for (auto& alias : argument.m_long_aliases) {
                    // TODO: case insensitive comparison
                    if (alias != arg)
                        continue;
                    if (argument.m_is_flag) {
                        argument.m_raw_arg = (char*)0x1; // Some dummy value
                        break;
                    }
                    argument.m_raw_arg = m_argv[++i];
                    break;
                }
            }
        }
    }

    for (auto& argument : m_arguments) {
        if (argument.m_required && !argument.has_value()) {
            return Error::with_message("Missing required argument {}", argument.m_long_aliases[0]);
        }
    }

    return {};
}

void CliArgs::print_usage()
{
    fmt::print("{}\n", m_description);
    fmt::print("Usage: {} [OPTIONS]\n\n", m_program_name);

    fmt::print("Options:\n");
    for (auto& argument : m_arguments)
        argument.print_usage();
}

void _cli_arg_parse(char const* arg, uint64_t* value) { *value = strtoull(arg, nullptr, 10); }

void _cli_arg_parse(char const* arg, int* value) { *value = strtol(arg, nullptr, 10); }

void _cli_arg_parse(char const* arg, std::string* value) { *value = arg; }
