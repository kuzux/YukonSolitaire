#pragma once

#include <math.h>

template <typename T> class AnimationController
{
public:
    AnimationController(T start, T target, float duration)
        : m_start(start)
        , m_target(target)
        , m_diff(target - start)
        , m_value(start)
        , m_duration(duration)
    {
    }

    void ease() { m_eased = true; }

    void set_target(T target)
    {
        m_target = target;
        m_diff = m_target - m_start;
    }

    void update(float dt)
    {
        m_elapsed += dt;
        if (m_elapsed > m_duration)
            m_elapsed = m_duration;

        float t = m_elapsed / m_duration;
        if (m_eased)
            t = -(cos(3.141592 * t) - 1.f) / 2.f;
        m_value = m_start + m_diff * t;
    }

    bool done() const { return m_elapsed >= m_duration; }
    T value() const { return m_value; }

private:
    T m_start;
    T m_target;

    T m_diff;
    T m_value;

    float m_duration;
    float m_elapsed { 0.0f };
    bool m_eased { false };
};
