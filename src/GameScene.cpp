#include "GameScene.h"

#include "AudioPlayer.h"
#include "GLArrayBuffer.h"
#include "GLFramework.h"
#include "GLShader.h"
#include "GLTexture.h"
#include "GLVertexArray.h"
#include "SceneStack.h"
#include "platform/platform.h"

#include "vendor/imgui.h"
// Generated file
#include "../dest/SpriteSheet.h"

#include <array>
#include <atomic>
#include <optional>
#include <thread>
#include <unistd.h>

using namespace std;

struct VertexInfo {
    glm::vec2 pos;
    glm::vec2 uv;
};

// clang-format off
static constexpr array<VertexInfo, 6> s_vertex_buffer_data = {{ 
    // vertex position         texture coords
    { { -1.f, -1.f },         { 0.f, 1.f } },
    { { 1.f,  -1.f },         { 1.f, 1.f } },
    { { -1.f,  1.f },         { 0.f, 0.f } },

    { { -1.f,  1.f },         { 0.f, 0.f } }, 
    { { 1.f,  -1.f },         { 1.f, 1.f } },
    { { 1.f,   1.f },         { 1.f, 0.f } } 
}};
// clang-format on

struct CardData {
    // position on the display
    glm::vec2 position;
    glm::vec2 scale;
    float z_index; // needs to be within [-1, 1). More negative = more in the front

    // position in the sprite sheet
    glm::vec2 uv_position;
    glm::vec2 uv_scale;
    float highlighted;
};

enum class SecondaryWindowMode {
    Regular,
    LayerView,
    UVView,
};

struct GameScene::Impl {
    vector<CardData> card_data;
    vector<CardData> empty_piles_data;

    GLVertexArray vao;
    GLArrayBuffer<VertexInfo> vbo { GLBufferTarget::ArrayBuffer };
    GLArrayBuffer<CardData> instance_vbo { GLBufferTarget::ArrayBuffer };

    GLVertexArray empty_piles_vao;
    GLArrayBuffer<CardData> empty_piles_instance_vbo { GLBufferTarget::ArrayBuffer };

    GLTexture sprite_sheet_texture { { 1536, 2048 } };
    SpriteSheet sprite_sheet;

    GLVertexArray fireworks_vao;

    GLProgram card_program;
    GLProgram empty_piles_program;
    GLProgram shadow_program;
    GLProgram fireworks_program;

    SecondaryWindowMode secondary_window_mode { SecondaryWindowMode::Regular };
    GLProgram layer_view_program;
    GLProgram uv_view_program;

    GLUniform<float> fireworks_aspect_ratio;
    GLUniform<float> fireworks_time;

    glm::vec2 drag_start_position;
    Emitter<SDLEvent>::Listener on_mouse_down;
    Emitter<SDLEvent>::Listener on_mouse_move;
    Emitter<SDLEvent>::Listener on_mouse_up;
    Emitter<SDLEvent>::Listener on_window_event;
    Emitter<SDLEvent>::Listener on_key_up;
    Emitter<SDLEvent>::Listener on_quit;
    Emitter<GameEvent>::Listener on_card_moved;
    Emitter<GameEvent>::Listener on_game_start;
    Emitter<GameEvent>::Listener on_game_won;

    std::optional<Sound> start_sound;
    std::optional<Sound> move_sound;
    std::optional<Sound> fireworks_sound;

    std::thread fireworks_sound_thread;
    std::atomic<bool> fireworks_sound_playing { false };

    bool display_new_game_popup { false };
    bool display_reset_game_popup { false };
};

GameScene::GameScene()
    : m_impl(make_unique<Impl>())
{
    {
        auto vao_binding = impl().vao.bind();
        {
            auto vbo_binding = impl().vbo.bind();
            MUST(impl().vbo.load(s_vertex_buffer_data));
            impl().vao.define_attribute(0, GLVertexAttribute(glm::vec2, VertexInfo, pos));
            impl().vao.define_attribute(1, GLVertexAttribute(glm::vec2, VertexInfo, uv));
        }
        {
            auto vbo_binding = impl().instance_vbo.bind();
            impl().vao.define_attribute(2, GLInstancedAttribute(glm::vec2, CardData, position));
            impl().vao.define_attribute(3, GLInstancedAttribute(glm::vec2, CardData, scale));
            impl().vao.define_attribute(4, GLInstancedAttribute(float, CardData, z_index));
            impl().vao.define_attribute(5, GLInstancedAttribute(glm::vec2, CardData, uv_position));
            impl().vao.define_attribute(6, GLInstancedAttribute(glm::vec2, CardData, uv_scale));
            impl().vao.define_attribute(7, GLInstancedAttribute(float, CardData, highlighted));
        }
    }

    {
        auto vao_binding = impl().empty_piles_vao.bind();
        {
            auto vbo_binding = impl().vbo.bind();
            MUST(impl().vbo.load(s_vertex_buffer_data));
            impl().empty_piles_vao.define_attribute(
                0, GLVertexAttribute(glm::vec2, VertexInfo, pos));
            impl().empty_piles_vao.define_attribute(
                1, GLVertexAttribute(glm::vec2, VertexInfo, uv));
        }
        {
            auto vbo_binding = impl().empty_piles_instance_vbo.bind();
            impl().empty_piles_vao.define_attribute(
                2, GLInstancedAttribute(glm::vec2, CardData, position));
            impl().empty_piles_vao.define_attribute(
                3, GLInstancedAttribute(glm::vec2, CardData, scale));
            impl().empty_piles_vao.define_attribute(
                4, GLInstancedAttribute(float, CardData, z_index));
            impl().empty_piles_vao.define_attribute(
                5, GLInstancedAttribute(glm::vec2, CardData, uv_position));
            impl().empty_piles_vao.define_attribute(
                6, GLInstancedAttribute(glm::vec2, CardData, uv_scale));
            impl().empty_piles_vao.define_attribute(
                7, GLInstancedAttribute(float, CardData, highlighted));
        }
    }

    {
        auto binding = impl().sprite_sheet_texture.bind(GL_TEXTURE0);
        MUST(impl().sprite_sheet_texture.load_from_file(get_resource_path("sheet.png")));
    }

    {
        auto vao_binding = impl().fireworks_vao.bind();
        {
            auto vbo_binding = impl().vbo.bind();
            MUST(impl().vbo.load(s_vertex_buffer_data));
            impl().fireworks_vao.define_attribute(0, GLVertexAttribute(glm::vec2, VertexInfo, pos));
            impl().fireworks_vao.define_attribute(1, GLVertexAttribute(glm::vec2, VertexInfo, uv));
        }
    }

    {
        auto vertex_shader = MUST(
            GLShader::from_file(get_resource_path("card.vert.glsl"), GLShader::Type::Vertex));
        auto fragment_shader = MUST(
            GLShader::from_file(get_resource_path("card.frag.glsl"), GLShader::Type::Fragment));

        impl().card_program.attach(vertex_shader);
        impl().card_program.attach(fragment_shader);
        MUST(impl().card_program.link());
    }

    {
        auto vertex_shader = MUST(
            GLShader::from_file(get_resource_path("card.vert.glsl"), GLShader::Type::Vertex));
        auto fragment_shader = MUST(GLShader::from_file(
            get_resource_path("empty_pile.frag.glsl"), GLShader::Type::Fragment));

        impl().empty_piles_program.attach(vertex_shader);
        impl().empty_piles_program.attach(fragment_shader);
        MUST(impl().empty_piles_program.link());
    }

    {
        auto vertex_shader = MUST(
            GLShader::from_file(get_resource_path("card.vert.glsl"), GLShader::Type::Vertex));
        auto fragment_shader = MUST(GLShader::from_file(
            get_resource_path("layer_view.frag.glsl"), GLShader::Type::Fragment));

        impl().layer_view_program.attach(vertex_shader);
        impl().layer_view_program.attach(fragment_shader);
        MUST(impl().layer_view_program.link());
    }

    {
        auto vertex_shader = MUST(
            GLShader::from_file(get_resource_path("card.vert.glsl"), GLShader::Type::Vertex));
        auto fragment_shader = MUST(
            GLShader::from_file(get_resource_path("uv_view.frag.glsl"), GLShader::Type::Fragment));

        impl().uv_view_program.attach(vertex_shader);
        impl().uv_view_program.attach(fragment_shader);
        MUST(impl().uv_view_program.link());
    }

    {
        auto vertex_shader = MUST(
            GLShader::from_file(get_resource_path("shadow.vert.glsl"), GLShader::Type::Vertex));
        auto fragment_shader = MUST(
            GLShader::from_file(get_resource_path("shadow.frag.glsl"), GLShader::Type::Fragment));

        impl().shadow_program.attach(vertex_shader);
        impl().shadow_program.attach(fragment_shader);
        MUST(impl().shadow_program.link());
    }

    {
        auto vertex_shader = MUST(
            GLShader::from_file(get_resource_path("fireworks.vert.glsl"), GLShader::Type::Vertex));
        auto fragment_shader = MUST(GLShader::from_file(
            get_resource_path("fireworks.frag.glsl"), GLShader::Type::Fragment));

        impl().fireworks_program.attach(vertex_shader);
        impl().fireworks_program.attach(fragment_shader);
        MUST(impl().fireworks_program.link());
    }

    impl().fireworks_aspect_ratio = impl().fireworks_program.get_uniform<float>("aspect_ratio");
    impl().fireworks_time = impl().fireworks_program.get_uniform<float>("time");

    m_game.new_game();
    impl().card_data.resize(52);

    auto initial_card_scale = default_card_scale();
    m_game.card_scale(initial_card_scale);

    for (auto* pile : m_game.all_piles()) {
        auto position = pile->position();
        impl().empty_piles_data.push_back(
            { position, initial_card_scale, 0.99f, { 0.f, 0.f }, { 0.f, 0.f }, 0.f });
    }
    {
        auto binding = impl().empty_piles_instance_vbo.bind();
        impl().empty_piles_instance_vbo.load(impl().empty_piles_data);
    }

    resize_card_sprites();

    impl().start_sound = MUST(Sound::load(get_resource_path("shuffle.opus"), Sound::Type::Sfx));
    impl().move_sound = MUST(Sound::load(get_resource_path("playcard.wav"), Sound::Type::Sfx));
    impl().fireworks_sound = MUST(Sound::load(get_resource_path("fw_01.wav"), Sound::Type::Sfx));
}

GameScene::~GameScene() = default;

glm::vec2 GameScene::default_card_scale() const
{
    auto& window = SDLWindow::the();
    float card_aspect_ratio = 140.f / 190.f;
    float window_aspect_ratio = window.aspect_ratio();

    float scale = card_aspect_ratio / window_aspect_ratio;
    return { .15f * scale, .15f };
}

void GameScene::resize_card_sprites()
{
    m_game.card_scale(default_card_scale());

    for (auto& card : impl().empty_piles_data)
        card.scale = default_card_scale();

    {
        auto binding = impl().empty_piles_instance_vbo.bind();
        impl().empty_piles_instance_vbo.load(impl().empty_piles_data);
    }
}

void GameScene::attach_to(SDLEventLoop& event_loop)
{
    auto& window = SDLWindow::the();

    impl().on_window_event = event_loop.on(SDL_WINDOWEVENT, [&](SDLEvent const& event) {
        if (event.window().event == SDL_WINDOWEVENT_RESIZED) {
            resize_card_sprites();
            update_aspect_ratio(SDLWindow::the().aspect_ratio());
        }
    });

    impl().on_mouse_down = event_loop.on(SDL_MOUSEBUTTONDOWN, [&](SDLEvent const& event) {
        auto& mouse_event = event.mouse_button();
        if (mouse_event.button != SDL_BUTTON_LEFT)
            return;
        glm::vec2 mouse_position = window.gl_coordinates({ mouse_event.x, mouse_event.y });
        m_game.mouse_down(mouse_position);
    });

    impl().on_mouse_move = event_loop.on(SDL_MOUSEMOTION, [&](SDLEvent const& event) {
        auto& mouse_event = event.mouse_motion();
        glm::vec2 mouse_position = window.gl_coordinates({ mouse_event.x, mouse_event.y });
        m_game.mouse_moved_to(mouse_position);
    });

    impl().on_mouse_up = event_loop.on(SDL_MOUSEBUTTONUP, [&](SDLEvent const& event) {
        auto& mouse_event = event.mouse_button();

        if (mouse_event.button == SDL_BUTTON_X1) {
            CardGame::the().undo();
        } else if (mouse_event.button == SDL_BUTTON_X2) {
            CardGame::the().redo();
        } else if (mouse_event.button == SDL_BUTTON_LEFT) {
            glm::vec2 mouse_position = window.gl_coordinates({ mouse_event.x, mouse_event.y });
            m_game.mouse_up(mouse_position);
        }
    });
    impl().on_key_up = event_loop.on(SDL_KEYUP, [&](SDLEvent const& event) {
        auto& key_event = event.key();
        bool ctrl_pressed = key_event.keysym.mod & (KMOD_LCTRL | KMOD_RCTRL);
        bool shift_pressed = key_event.keysym.mod & (KMOD_LSHIFT | KMOD_RSHIFT);
        bool cmd_pressed = key_event.keysym.mod & KMOD_LGUI;

#ifdef __APPLE__
        bool modifier_pressed = cmd_pressed;
        (void)ctrl_pressed;
#else
        bool modifier_pressed = ctrl_pressed;
        (void)cmd_pressed;
#endif

        if (key_event.keysym.sym == SDLK_z && modifier_pressed && !shift_pressed)
            CardGame::the().undo();
        if (key_event.keysym.sym == SDLK_z && modifier_pressed && shift_pressed)
            CardGame::the().redo();

        if (key_event.keysym.sym == SDLK_n && modifier_pressed)
            impl().display_new_game_popup = true;
        if (key_event.keysym.sym == SDLK_r && modifier_pressed)
            impl().display_reset_game_popup = true;
    });

    impl().on_quit
        = event_loop.on(SDL_QUIT, [&](SDLEvent const&) { m_game.record_current_game(); });

    impl().on_card_moved = m_game.on(GameEvent::Type::CardMoved, [&](GameEvent const&) {
        fmt::print("Card moved\n");
        MUST(SDLWindow::the().audio_player().play(*impl().move_sound));
    });
    impl().on_game_start = m_game.on(GameEvent::Type::Started, [&](GameEvent const&) {
        fmt::print("Game started\n");
        MUST(SDLWindow::the().audio_player().play(*impl().start_sound));
        impl().fireworks_sound_playing = false;
    });
    impl().on_game_won = m_game.on(GameEvent::Type::GameWon, [&](GameEvent const&) {
        fmt::print("Game won\n");
        impl().fireworks_sound_playing = true;
        impl().fireworks_sound_thread = std::thread([this] {
            int sleep_ms = 1000;
            usleep(50 * 1000);
            for (;;) {
                if (!impl().fireworks_sound_playing)
                    break;
                MUST(SDLWindow::the().audio_player().play(*impl().fireworks_sound));
                usleep(sleep_ms * 1500);
            }
        });
    });

    update_aspect_ratio(SDLWindow::the().aspect_ratio());
}

void GameScene::update_aspect_ratio(float aspect_ratio)
{
    auto program_binding = impl().fireworks_program.bind();
    impl().fireworks_aspect_ratio.set_value(aspect_ratio);
}

extern bool g_has_secondary;

void GameScene::detach()
{
    impl().on_mouse_down.off();
    impl().on_mouse_move.off();
    impl().on_mouse_up.off();
    impl().on_window_event.off();
    impl().on_key_up.off();
    impl().on_quit.off();

    impl().on_card_moved.off();
    impl().on_game_start.off();
    impl().on_game_won.off();

    SDLWindow::the().audio_player().stop(*impl().start_sound);
    SDLWindow::the().audio_player().stop(*impl().move_sound);
    SDLWindow::the().audio_player().stop(*impl().fireworks_sound);
}

void GameScene::draw(SDLFrameInfo const& frame)
{
    m_game.tick_time(frame.delta_seconds());

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    for (Card& card : m_game.deck()) {
        auto& card_data = impl().card_data[card.numeric_value()];
        if (card.position_dirty()) {
            card_data.position = card.position();
            card_data.scale = default_card_scale() * glm::vec2(card.x_scale(), 1.f);
            card_data.z_index = card.z_index();
            card_data.highlighted = card.highlighted() ? 1.f : 0.f;
        }
        if (card.sprite_dirty()) {
            auto tex = MUST(impl().sprite_sheet.get_subtexture(card.sprite_name()));
            card_data.uv_position = tex.position;
            card_data.uv_scale = tex.size;
        }
        card.updated_view_model();

        {
            auto binding = impl().instance_vbo.bind();
            impl().instance_vbo.load(impl().card_data);
        }
    }

    {
        auto program_binding = impl().card_program.bind();
        auto vao_binding = impl().vao.bind();
        auto texture_binding = impl().sprite_sheet_texture.bind(GL_TEXTURE0);

        glDrawArraysInstanced(GL_TRIANGLES, 0, 6, impl().card_data.size());
    }
    {
        auto program_binding = impl().empty_piles_program.bind();
        auto vao_binding = impl().empty_piles_vao.bind();

        glDrawArraysInstanced(GL_TRIANGLES, 0, 6, impl().empty_piles_data.size());
    }
    {
        auto program_binding = impl().shadow_program.bind();
        auto vao_binding = impl().vao.bind();

        glDrawArraysInstanced(GL_TRIANGLES, 0, 6, impl().card_data.size());
    }

    if (m_game.game_won()) {
        auto program_binding = impl().fireworks_program.bind();
        impl().fireworks_time.set_value(m_game.time_after_winning());
        auto vao_binding = impl().fireworks_vao.bind();

        glDrawArrays(GL_TRIANGLES, 0, 6);
    }
}

void GameScene::draw_secondary(SDLFrameInfo const& frame)
{
    (void)frame;
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto vao_binding = impl().vao.bind();
    auto texture_binding = impl().sprite_sheet_texture.bind(GL_TEXTURE0);

    if (impl().secondary_window_mode == SecondaryWindowMode::LayerView) {
        auto program_binding = impl().layer_view_program.bind();
        glDrawArraysInstanced(GL_TRIANGLES, 0, 6, impl().card_data.size());
    }

    if (impl().secondary_window_mode == SecondaryWindowMode::Regular) {
        auto program_binding = impl().card_program.bind();
        glDrawArraysInstanced(GL_TRIANGLES, 0, 6, impl().card_data.size());
    }

    if (impl().secondary_window_mode == SecondaryWindowMode::UVView) {
        auto program_binding = impl().uv_view_program.bind();
        glDrawArraysInstanced(GL_TRIANGLES, 0, 6, impl().card_data.size());
    }
}

void GameScene::draw_ui()
{
    if (g_has_secondary) {
        ImGui::Begin("Secondary Window");
        if (ImGui::Button("Regular"))
            impl().secondary_window_mode = SecondaryWindowMode::Regular;
        ImGui::SameLine();
        if (ImGui::Button("Layer View"))
            impl().secondary_window_mode = SecondaryWindowMode::LayerView;
        ImGui::SameLine();
        if (ImGui::Button("UV View"))
            impl().secondary_window_mode = SecondaryWindowMode::UVView;
        ImGui::End();
    }

    if (impl().display_new_game_popup) {
        ImGui::Begin("New Game");
        ImGui::Text("Are you sure you want to start a new game?");
        if (ImGui::Button("Yes")) {
            CardGame::the().new_game();
            impl().display_new_game_popup = false;
        }
        ImGui::SameLine();
        if (ImGui::Button("No"))
            impl().display_new_game_popup = false;
        ImGui::End();
    }

    if (impl().display_reset_game_popup) {
        ImGui::Begin("Reset Game");
        ImGui::Text("Are you sure you want to reset the game?");
        if (ImGui::Button("Yes")) {
            CardGame::the().reset_game();
            impl().display_reset_game_popup = false;
        }
        ImGui::SameLine();
        if (ImGui::Button("No"))
            impl().display_reset_game_popup = false;
        ImGui::End();
    }

    ImGui::Begin("Game Menu");

    ImGui::BeginDisabled(!CardGame::the().can_undo());
    if (ImGui::Button("Undo"))
        CardGame::the().undo();
    ImGui::EndDisabled();

    ImGui::SameLine();

    ImGui::BeginDisabled(!CardGame::the().can_redo());
    if (ImGui::Button("Redo"))
        CardGame::the().redo();
    ImGui::EndDisabled();

    ImGui::BeginDisabled(!CardGame::the().can_auto_win());
    if (ImGui::Button("Auto Finish"))
        CardGame::the().auto_win();
    ImGui::EndDisabled();

    int elapsed_seconds = CardGame::the().game_time();
    ImGui::Text("Elapsed time: %02d:%02d", elapsed_seconds / 60, elapsed_seconds % 60);

    ImGui::Dummy({ 0.f, 10.f });

    if (ImGui::Button("Copy game seed"))
        ImGui::SetClipboardText(std::to_string(CardGame::the().game_seed()).c_str());

    if (ImGui::Button("New Game"))
        CardGame::the().new_game();
    if (ImGui::Button("Reset Game"))
        CardGame::the().reset_game();

    if (ImGui::Button("Main Menu")) {
        detach();
        CardGame::the().reset_game();
        SceneStack::the().pop();
    }

    ImGui::End();
}
