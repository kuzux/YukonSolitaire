#include "GLShader.h"

#include <fstream>
#include <sstream>
#include <vector>

#include "GLFramework.h"

#include "Defer.h"

using namespace std;

ErrorOr<GLShader> GLShader::from_file(string const& filename, Type type)
{
    ifstream source_file(filename);
    if (!source_file.is_open())
        return Error::with_message(fmt::format("Could not open file {}", filename));
    stringstream sst;
    sst << source_file.rdbuf();
    GLShader res { sst.str(), type };

    // This hackery was needed to make both vscode and g++ happy
#ifndef __INTELLISENSE__
    if (res.compile())
        return res;
#else
    if (res.compile())
        return move(res);
#endif

    string message = fmt::format("In file {}: \n{}", filename, res.error_message());
    return Error::with_message(message);
}

GLShader::GLShader(GLShader&& shader)
    : m_type(shader.m_type)
    , m_source(std::move(shader.m_source))
    , m_glid(shader.m_glid)
    , m_is_compiled(shader.m_is_compiled)
{
    shader.m_glid = 0;
    shader.m_is_compiled = false;
}

GLShader::GLShader(string_view source, Type type)
    : m_type(type)
    , m_source(source)
{
    GLenum gl_type = (type == Type::Fragment) ? GL_FRAGMENT_SHADER : GL_VERTEX_SHADER;
    m_glid = glCreateShader(gl_type);
    GLint length = source.length();
    const char* cstr = m_source.c_str();
    glShaderSource(m_glid, 1, &cstr, &length);
}

GLShader::~GLShader()
{
    if (m_glid == 0)
        return;
    glDeleteShader(m_glid);
}

GLuint GLShader::gl_id() const
{
    assert(m_is_compiled);
    return m_glid;
}

bool GLShader::compile()
{
    assert(!m_is_compiled);
    glCompileShader(m_glid);

    GLint compile_status = GL_FALSE;
    glGetShaderiv(m_glid, GL_COMPILE_STATUS, &compile_status);
    if (compile_status == GL_TRUE) {
        m_is_compiled = true;
        return true;
    }

    return false;
}

string GLShader::error_message()
{
    GLint length;
    glGetShaderiv(m_glid, GL_INFO_LOG_LENGTH, &length);
    if (length == 0) {
        return "";
    }
    vector<char> logbuf(length + 1);
    glGetShaderInfoLog(m_glid, length, NULL, logbuf.data());
    return string { logbuf.begin(), logbuf.end() };
}

void _gl_uniform_set(GLuint location, size_t count, int* val)
{
    glUniform1iv(location, count, val);
}

void _gl_uniform_set(GLuint location, size_t count, float* val)
{
    glUniform1fv(location, count, val);
}

void _gl_uniform_set(GLuint location, size_t count, glm::vec2* val)
{
    glUniform2fv(location, count, reinterpret_cast<float*>(val));
}

void _gl_uniform_set(GLuint location, size_t count, glm::vec3* val)
{
    glUniform3fv(location, count, reinterpret_cast<float*>(val));
}

void _gl_uniform_set(GLuint location, size_t count, glm::ivec2* val)
{
    glUniform2iv(location, count, reinterpret_cast<int*>(val));
}

void _gl_uniform_set(GLuint location, size_t count, glm::ivec3* val)
{
    glUniform3iv(location, count, reinterpret_cast<int*>(val));
}

void _gl_uniform_set(GLuint location, size_t count, glm::mat4* val)
{
    glUniformMatrix4fv(location, count, false, reinterpret_cast<float*>(val));
}

GL::int_ _gl_program_in_use()
{
    GL::int_ program_in_use;
    glGetIntegerv(GL_CURRENT_PROGRAM, &program_in_use);
    return program_in_use;
}

ErrorOr<GL::int_> GLProgram::get_uniform_location(string_view name) const
{
    string name_copy(name);
    GL::int_ location = glGetUniformLocation(m_glid, name_copy.c_str());
    if (location == GL_INVALID_OPERATION || location == GL_INVALID_VALUE)
        return Error::with_message(
            fmt::format("glGetUniformLocation failed with error {}", glGetError()));

    if (location < 0)
        fmt::print(stderr, "WARNING: Invalid uniform name {} (maybe optimized out?)\n", name);
    return location;
}

GLProgram::GLProgram() { m_glid = glCreateProgram(); }

GLProgram::GLProgram(GLProgram&& program)
    : m_glid(program.m_glid)
    , m_attached_shaders(program.m_attached_shaders)
    , m_is_linked(program.m_is_linked)
{
    program.m_glid = 0;
    program.m_attached_shaders.clear();
    program.m_is_linked = false;
}

GLProgram::~GLProgram()
{
    if (m_glid == 0)
        return;
    glDeleteProgram(m_glid);
}

void GLProgram::attach(GLShader const& shader)
{
    assert(!m_is_linked);

    glAttachShader(m_glid, shader.gl_id());
    m_attached_shaders.push_back(shader.gl_id());
}

ErrorOr<void> GLProgram::link()
{
    assert(!m_attached_shaders.empty());
    assert(!m_is_linked);

    Defer detach([this] {
        for (auto shader_id : m_attached_shaders)
            glDetachShader(m_glid, shader_id);
        m_attached_shaders.clear();
    });

    glLinkProgram(m_glid);
    GLint link_status = GL_FALSE;
    glGetProgramiv(m_glid, GL_LINK_STATUS, &link_status);
    if (link_status == GL_FALSE) {
        GLint length;
        glGetProgramiv(m_glid, GL_INFO_LOG_LENGTH, &length);
        vector<char> logbuf(length + 1);
        glGetProgramInfoLog(m_glid, length, NULL, logbuf.data());
        string error_message { logbuf.begin(), logbuf.end() };
        return Error::with_message(error_message);
    }

    m_is_linked = true;
    return {};
}

GLProgram::Binding::Binding(GLProgram& program)
    : m_program(program)
{
    GLint program_in_use;
    glGetIntegerv(GL_CURRENT_PROGRAM, &program_in_use);
    assert(program_in_use >= 0);
    m_old_glid = program_in_use;

    if (m_old_glid != m_program.m_glid)
        glUseProgram(m_program.m_glid);
}

GLProgram::Binding::~Binding()
{
    GLint program_in_use;
    glGetIntegerv(GL_CURRENT_PROGRAM, &program_in_use);
    assert(program_in_use == (GLint)m_program.m_glid);

    if (m_old_glid != m_program.m_glid)
        glUseProgram(m_old_glid);
}

GLProgram::Binding GLProgram::bind() { return Binding(*this); }
