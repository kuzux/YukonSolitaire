#pragma once
#include <glm/glm.hpp>
#include <string>

#include "Error.h"
#include "GLTypes.h"

class GLTexture
{
public:
    struct Binding {
        friend class GLTexture;
        ~Binding();

    private:
        Binding(GLTexture& tex, GL::enum_ texture_unit);

        GLTexture& m_tex;
        GL::uint m_old_glid { 0 };
        GL::enum_ m_texture_unit { 0 };
    };

    GLTexture(glm::ivec2 size);
    ~GLTexture();

    [[nodiscard]] Binding bind(GL::enum_ texture_unit);

    glm::ivec2 size() const { return m_size; }
    void set_data(const void* data, GL::enum_ format, GL::enum_ type);
    ErrorOr<void> load_from_file(std::string const& filename);

private:
    bool m_is_bound { false };
    glm::ivec2 m_size;
    GL::uint m_glid { 0 };
};
