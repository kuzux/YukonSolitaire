#include "Pile.h"

#include <assert.h>
#include <fmt/core.h>

void Pile::add_cards(std::vector<Card>& cards, size_t offset, size_t length)
{
    for (size_t i = 0; i < length; i++) {
        m_cards.push_back(&cards[offset + i]);
        cards[offset + i].pile(*this);
    }
}

void Pile::reveal_top(size_t N)
{
    if (N > m_cards.size())
        N = m_cards.size();
    for (size_t i = 0; i < N; i++)
        m_cards[m_cards.size() - i - 1]->reveal();
}

void Pile::lay_out_cards()
{
    auto curr_pos = m_topmost_position;

    float dy = 0.1f;
    if (m_cards.size() > 15)
        dy = 1.5f / m_cards.size();

    for (size_t i = 0; i < m_cards.size(); i++) {
        m_cards[i]->move_to(curr_pos);
        m_cards[i]->move_to_layer(i + 1);
        curr_pos.y -= dy;
    }
}

bool Pile::can_add_group_of_cards(GroupOfCards const& group) const
{
    auto const& first_card = group.first_card();
    if (m_cards.empty())
        return first_card.rank().value() == 13;
    auto topmost_card = m_cards.back();
    return topmost_card->can_add_card_below(first_card);
}

GroupOfCards Pile::cards_starting_from(Card const& starting_card)
{
    auto it = std::find(m_cards.begin(), m_cards.end(), &starting_card);
    assert(it != m_cards.end());

    size_t offset = it - m_cards.begin();
    size_t length = m_cards.size() - offset;

    GroupOfCards group { *this, offset, length };
    if (offset > 0 && !m_cards[offset - 1]->revealed())
        group.will_reveal_a_card();

    return group;
}

void Pile::add_group_of_cards(GroupOfCards const& group)
{
    auto* other_pile = &group.source_pile();
    auto other_offset = group.source_offset();
    if (other_pile == this) {
        other_pile = &group.destination_pile();
        other_offset = group.destination_offset();
        assert(&group.source_pile() == this);
    } else {
        assert(&group.destination_pile() == this);
    }

    if (group.revealed_a_card()) {
        assert(!m_cards.empty());
        m_cards.back()->hide();
    }

    for (size_t i = 0; i < group.length(); i++) {
        auto* card = other_pile->cards()[other_offset + i];
        m_cards.push_back(card);
        card->pile(*this);
    }
}

void Pile::remove_group_of_cards(GroupOfCards const& group)
{
    assert(&group.destination_pile() == this || &group.source_pile() == this);
    for (size_t i = 0; i < group.length(); i++)
        m_cards.pop_back();

    if (!m_cards.empty())
        m_cards.back()->reveal();
}
