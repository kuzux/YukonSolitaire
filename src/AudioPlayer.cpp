#include "AudioPlayer.h"
#include "Defer.h"
#include "SDLWindow.h"

#include <algorithm>
#include <opusfile.h>

AudioPlayer::PlayingSound::PlayingSound(Sound const& sound, AudioPlayer& player, bool looping)
    : m_sound(&sound)
    , m_looping(looping)
{
    auto src_spec = sound.m_spec;
    auto dst_spec = player.m_spec;

    m_stream = SDL_NewAudioStream(src_spec.format, src_spec.channels, src_spec.freq,
        dst_spec.format, dst_spec.channels, dst_spec.freq);
    if (m_stream == nullptr) {
        fmt::print(stderr, "SDL_NewAudioStream: {}\n", SDL_GetError());
        return;
    }

    resample_more();

    m_has_stream = true;
}

AudioPlayer::PlayingSound::~PlayingSound()
{
    // Occasionally, the resample_more method of this class are called after destruction (by audio
    // thread callback) To detect this is the case, we set m_stopped to true on destruction and
    // check for it in that method
    m_stopped = true;
    if (m_has_stream)
        SDL_FreeAudioStream(m_stream);
}

void AudioPlayer::PlayingSound::reset()
{
    m_play_cursor = 0;
    if (m_stream)
        SDL_AudioStreamClear(m_stream);
    m_resample_cursor = 0;
    resample_more();
}

AudioPlayer::PlayingSound::PlayingSound(PlayingSound&& other)
{
    std::swap(m_sound, other.m_sound);
    std::swap(m_stream, other.m_stream);
    std::swap(m_play_cursor, other.m_play_cursor);
    std::swap(m_resample_cursor, other.m_resample_cursor);
    std::swap(m_stopped, other.m_stopped);
    std::swap(m_looping, other.m_looping);
    std::swap(m_has_stream, other.m_has_stream);
}

AudioPlayer::PlayingSound& AudioPlayer::PlayingSound::operator=(PlayingSound&& other)
{
    if (this != &other) {
        std::swap(m_sound, other.m_sound);
        std::swap(m_stream, other.m_stream);
        std::swap(m_play_cursor, other.m_play_cursor);
        std::swap(m_resample_cursor, other.m_resample_cursor);
        std::swap(m_stopped, other.m_stopped);
        std::swap(m_looping, other.m_looping);
        std::swap(m_has_stream, other.m_has_stream);
    }

    return *this;
}

void AudioPlayer::PlayingSound::resample_more()
{
    if (m_stopped)
        return;

    size_t total_bytes = m_sound->m_length;
    if (m_resample_cursor == total_bytes) {
        if (!m_looping)
            SDL_AudioStreamFlush(m_stream);
        else
            m_resample_cursor = 0;
    }
    size_t total_remaining_bytes = total_bytes - m_resample_cursor;
    size_t bytes_to_resample = std::min((size_t)(1 << 15), total_remaining_bytes);
    int rc = SDL_AudioStreamPut(m_stream, m_sound->m_buffer + m_resample_cursor, bytes_to_resample);
    if (rc == -1) {
        fmt::print(stderr, "SDL_AudioStreamPut: {}\n", SDL_GetError());
    }
    m_resample_cursor += bytes_to_resample;
}

bool AudioPlayer::PlayingSound::finished() const
{
    return m_stopped || (m_play_cursor >= m_sound->num_samples() && !m_looping);
}

void AudioPlayer::sdl_audio_callback(void* userdata, uint8_t* stream, int len)
{
    AudioPlayer& player = *static_cast<AudioPlayer*>(userdata);
    size_t frame_count = len / player.bytes_per_frame();
    memset(stream, 0, len);

    if (player.m_paused)
        return;

    if (!SDLEventLoop::running())
        return;

    player.m_playback_offset += frame_count;

    {
        std::lock_guard lock(player.m_sound_queue_mutex);
        while (!player.m_sound_queue.empty()) {
            auto curr = player.m_sound_queue.front();
            if (curr.starting_offset > player.m_playback_offset)
                break;
            player.m_playing_sounds.emplace_back(*curr.sound, player, curr.looping);
            player.m_sound_queue.pop_front();
        }
    }

    if (player.m_playing_sounds.empty()) {
        return;
    }

    for (auto& now_playing : player.m_playing_sounds) {
        auto* playing_stream = now_playing.stream();
        if (playing_stream == nullptr) {
            now_playing.stop();
            continue;
        }

        if (now_playing.finished()) {
            if (now_playing.looping()) {
                now_playing.reset();
            } else {
                continue;
            }
        }

        int available = now_playing.available_resampled_bytes();
        uint32_t length_to_play = std::min(len, available);

        static float tmpbuf[1 << 16];
        int rc = SDL_AudioStreamGet(playing_stream, (uint8_t*)tmpbuf, length_to_play);
        if (rc == -1) {
            fmt::print(stderr, "SDL_AudioStreamGet: {}\n", SDL_GetError());
            memset(stream, 0, len);
            return;
        }
        if (now_playing.available_resampled_bytes() < (size_t)(1 << 13))
            now_playing.resample_more();

        if (rc == 0)
            continue;

        float* dest = (float*)stream;
        size_t length_in_samples = rc / sizeof(float);
        now_playing.advance_cursor(length_in_samples);

        // TODO: We might want to add a "knee" to the volume curve
        //     when adding multiple signals together
        if (now_playing.type() == Sound::Type::Music) {
            for (uint32_t i = 0; i < length_in_samples; i++)
                dest[i] += tmpbuf[i] * player.m_music_volume;
        } else if (now_playing.type() == Sound::Type::Sfx) {
            for (uint32_t i = 0; i < length_in_samples; i++)
                dest[i] += tmpbuf[i] * player.m_sfx_volume;
        }
    }
    player.m_playing_sounds.erase(
        std::remove_if(player.m_playing_sounds.begin(), player.m_playing_sounds.end(),
            [](PlayingSound const& sound) { return sound.finished(); }),
        player.m_playing_sounds.end());
}

ErrorOr<void> AudioPlayer::initialize()
{
    SDL_AudioSpec want;
    SDL_zero(want);
    want.freq = 44100;
    want.format = AUDIO_F32SYS;
    want.channels = 2;
    want.samples = 1024;
    want.callback = AudioPlayer::sdl_audio_callback;
    want.userdata = this;

    m_device_id = SDL_OpenAudioDevice(nullptr, 0, &want, &m_spec, 0);
    if (m_device_id == 0)
        return Error::with_message("SDL_OpenAudioDevice: {}", SDL_GetError());

    SDL_PauseAudioDevice(m_device_id, 0);

    return {};
}

ErrorOr<void> AudioPlayer::play_after(Sound const& sound, size_t ms, bool loop)
{
    if (!initialized())
        return Error::with_message("AudioPlayer not initialized");
    if (!sound.loaded())
        return Error::with_message("Audio not loaded");

    std::lock_guard lock(m_sound_queue_mutex);
    size_t offset_in_frames = ms_to_frame_count(ms);

    m_sound_queue.emplace_back(sound, m_playback_offset + offset_in_frames, loop);

    return {};
}

ErrorOr<Sound> Sound::load(std::string const& path, Type type)
{
    Sound sound;
    SDL_AudioSpec spec;
    uint8_t* buffer = nullptr;
    uint32_t length = 0;

    if (path.ends_with(".wav")) {
        if (SDL_LoadWAV(path.c_str(), &spec, &buffer, &length) == nullptr)
            return Error::with_message("SDL_LoadWAV: {}", SDL_GetError());
        sound.m_from_sdl = true;
    } else if (path.ends_with(".opus")) {
        int rc;
        OggOpusFile* opus = op_open_file(path.c_str(), &rc);
        if (rc != 0) {
            return Error::with_message("op_open_file: {}", rc);
        }
        Defer defer_opus_close([&] { op_free(opus); });

        size_t bufsize = op_pcm_total(opus, -1) * 2 * sizeof(float);
        buffer = new uint8_t[bufsize];
        float* float_buf = (float*)buffer;
        int samples_read = 0;
        for (;;) {
            int samples_per_channel
                = op_read_float_stereo(opus, float_buf + samples_read, bufsize - samples_read);
            if (samples_per_channel < 0)
                return Error::with_message("op_read_float_stereo: {}", samples_per_channel);
            if (samples_per_channel == 0)
                break;
            samples_read += samples_per_channel * 2;
        }

        spec.freq = 48000;
        spec.format = AUDIO_F32SYS;
        spec.channels = 2;
        spec.samples = samples_read / 2;

        length = samples_read * sizeof(float);
        sound.m_from_sdl = false;
    } else {
        return Error::with_message("Unsupported audio format");
    }

    sound.m_spec = spec;
    sound.m_buffer = buffer;
    sound.m_length = length;
    sound.m_type = type;
    sound.m_path = path;
    fmt::print("loaded sound with path {}\n", sound.m_path);

    return ErrorOr<Sound> { std::move(sound) };
}

Sound::~Sound()
{
    if (!m_buffer)
        return;

    if (m_from_sdl)
        SDL_FreeWAV(m_buffer);
    else
        delete[] m_buffer;
}

Sound::Sound(Sound&& other)
{
    m_spec = other.m_spec;
    m_buffer = other.m_buffer;
    m_length = other.m_length;
    m_from_sdl = other.m_from_sdl;
    m_type = other.m_type;
    m_path = std::move(other.m_path);

    other.m_buffer = nullptr;
    other.m_length = 0;
}

Sound& Sound::operator=(Sound&& other)
{
    if (this != &other) {
        m_spec = other.m_spec;
        m_buffer = other.m_buffer;
        m_length = other.m_length;
        m_from_sdl = other.m_from_sdl;
        m_type = other.m_type;
        m_path = std::move(other.m_path);

        other.m_buffer = nullptr;
        other.m_length = 0;
    }

    return *this;
}

void AudioPlayer::stop_all()
{
    bool old_pause_value = m_paused;
    m_paused = true;
    m_sound_queue.clear();
    m_playing_sounds.clear();
    m_paused = old_pause_value;
}

void AudioPlayer::stop(Sound const& sound)
{
    for (auto& playing : m_playing_sounds) {
        if (&playing.sound() == &sound)
            playing.stop();
    }

    std::lock_guard lock(m_sound_queue_mutex);
    m_sound_queue.erase(std::remove_if(m_sound_queue.begin(), m_sound_queue.end(),
                            [&](QueuedSound const& queued) { return queued.sound == &sound; }),
        m_sound_queue.end());
}