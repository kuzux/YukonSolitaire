#pragma once

#include <glm/glm.hpp>

#include <array>
#include <functional>
#include <string>
#include <string_view>
#include <vector>

#include "Error.h"
#include "GLTypes.h"

class GLShader
{
public:
    enum class Type { Vertex, Fragment };

    static ErrorOr<GLShader> from_file(std::string const& filename, Type type);
    ~GLShader();

    uint32_t gl_id() const;

    GLShader(GLShader&& shader);

private:
    GLShader(std::string_view source, Type type);
    bool compile();
    std::string error_message();

    Type m_type;
    std::string m_source;
    GL::uint m_glid { 0 };
    bool m_is_compiled { false };
};

void _gl_uniform_set(GL::uint location, size_t count, int* val);
void _gl_uniform_set(GL::uint location, size_t count, float* val);
void _gl_uniform_set(GL::uint location, size_t count, glm::vec2* val);
void _gl_uniform_set(GL::uint location, size_t count, glm::vec3* val);
void _gl_uniform_set(GL::uint location, size_t count, glm::ivec2* val);
void _gl_uniform_set(GL::uint location, size_t count, glm::ivec3* val);
void _gl_uniform_set(GL::uint location, size_t count, glm::mat4* val);

GL::int_ _gl_program_in_use();

template <typename T> class GLUniform
{
    friend class GLProgram;

public:
    GLUniform() = default;
    ~GLUniform() { }

    void set_value(T val)
    {
        assert(valid());
        auto program_in_use = _gl_program_in_use();
        assert(program_in_use == (GL::int_)m_program_id);

        _gl_uniform_set(m_location, 1, &val);
    }
    bool valid() const { return m_valid; }

private:
    GLUniform(GL::int_ location, GL::int_ program_id)
        : m_valid(true)
        , m_location(location)
        , m_program_id(program_id)
    {
    }
    bool m_valid { false };
    GL::uint m_location;
    GL::uint m_program_id;
};

template <typename T, size_t N> class GLUniform<std::array<T, N>>
{
    friend class GLProgram;

public:
    ~GLUniform() { }

    void set_value(std::array<T, N> const& val)
    {
        auto program_in_use = _gl_program_in_use();
        assert(program_in_use == (GL::int_)m_program_id);

        _gl_uniform_set(m_location, N, const_cast<T*>(val.data()));
    }

private:
    GLUniform(GL::int_ location, GL::int_ program_id)
        : m_location(location)
        , m_program_id(program_id)
    {
    }
    GL::uint m_location;
    GL::uint m_program_id;
};

class GLProgram
{
public:
    struct Binding {
        friend class GLProgram;
        Binding(GLProgram& program);
        ~Binding();

    private:
        GLProgram& m_program;
        GL::uint m_old_glid { 0 };
    };

    // TODO: Add a varargs of shader references here
    GLProgram();
    ~GLProgram();
    GLProgram(GLProgram&& program);

    void attach(GLShader const& shader);
    ErrorOr<void> link();
    [[nodiscard]] Binding bind();

    template <typename T> GLUniform<T> get_uniform(std::string_view name) const
    {
        assert(m_is_linked);
        auto location = get_uniform_location(name);
        if (location.is_error())
            assert(false && "Can't create uniform: Invalid program object");

        return GLUniform<T> { location.value(), (GL::int_)m_glid };
    }

private:
    ErrorOr<GL::int_> get_uniform_location(std::string_view name) const;

    GL::uint m_glid { 0 };
    std::vector<GL::uint> m_attached_shaders;
    bool m_is_linked { false };
};
