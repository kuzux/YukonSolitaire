#pragma once

#include "Card.h"
#include <glm/glm.hpp>

class AbstractPile;

class GroupOfCards
{
public:
    GroupOfCards(AbstractPile& source_pile, size_t offset, size_t length)
        : m_source_pile(source_pile)
        , m_source_offset(offset)
        , m_length(length)
    {
    }

    AbstractPile& source_pile() const { return m_source_pile; }
    size_t source_offset() const { return m_source_offset; }

    bool has_destination() const { return m_destination_pile != nullptr; }

    void will_reveal_a_card() { m_will_reveal_card = true; }
    bool revealed_a_card() const;

    AbstractPile& destination_pile() const { return *m_destination_pile; }
    size_t destination_offset() const { return m_destination_offset; }

    size_t length() const { return m_length; }
    Card& first_card() const;
    void move_to(AbstractPile& destination_pile);
    void move_by(glm::vec2 const& offset);

    void undo_move();
    void redo_move();

    void start_dragging();
    void cancel_drag();
    void drop_on_pile(AbstractPile& pile);

private:
    AbstractPile& m_source_pile;
    size_t m_source_offset;
    size_t m_length;

    AbstractPile* m_destination_pile { nullptr };
    size_t m_destination_offset { 0 };
    bool m_done { false };
    bool m_will_reveal_card { false };
};
