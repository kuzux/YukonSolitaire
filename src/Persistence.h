#pragma once

#include "Error.h"
#include <string>
#include <vector>

class Persistence
{
public:
    struct Stats {
        size_t games_played;
        size_t games_won;
        float win_ratio;
        float avg_winning_time;
        size_t seconds_played;
    };

    static Persistence& the();

    ErrorOr<void> load_database(std::string const& path);
    bool loaded() const { return m_db != nullptr; }

    void add_game_data(uint64_t game_seed, int seconds_played, int number_of_moves, bool won);
    std::vector<int> shortest_times(size_t count);
    Stats get_stats();

    ErrorOr<void> save_setting(std::string const& key, bool value)
    {
        return save_setting(key, value ? 1 : 0);
    }
    ErrorOr<void> save_setting(std::string const& key, int value);
    ErrorOr<void> save_setting(std::string const& key, float value);
    ErrorOr<void> save_setting(std::string const& key, std::string const& value);

    template <typename T> ErrorOr<T> load_setting(std::string const& key)
    {
        T value;
        TRY(load_setting(key, value));
        return value;
    }

private:
    inline static Persistence* s_the { nullptr };
    Persistence() = default;
    ~Persistence();

    ErrorOr<void> create_schema();

    ErrorOr<void> load_setting(std::string const& key, bool& value);
    ErrorOr<void> load_setting(std::string const& key, int& value);
    ErrorOr<void> load_setting(std::string const& key, float& value);
    ErrorOr<void> load_setting(std::string const& key, std::string& value);

    void* m_db { nullptr };
};
