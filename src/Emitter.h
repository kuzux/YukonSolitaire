#pragma once

#include <concepts>
#include <functional>
#include <unordered_map>

#if __cplusplus >= 202002L
template <typename Event>
concept EventLike = requires(Event const e) {
    { e.type() } -> std::same_as<typename Event::Type>;
};
#else
#define EventLike typename
#endif

template <EventLike Event> class Emitter
{
public:
    using Callback = std::function<void(Event const&)>;
    using EventType = typename Event::Type;

    class Listener
    {
        using CallbackId = typename std::unordered_multimap<EventType, Callback>::iterator;

    public:
        Listener()
            : m_parent(nullptr)
        {
        }

        Listener(Emitter& parent, CallbackId id)
            : m_parent(&parent)
            , m_id(id)
        {
        }

        ~Listener()
        {
            if (m_parent)
                off();
        }

        Listener& operator=(Listener&& other)
        {
            m_parent = other.m_parent;
            m_id = other.m_id;
            other.m_parent = nullptr;
            other.m_id = {};
            return *this;
        }

        void off()
        {
            if (!m_parent)
                return;
            m_parent->m_callbacks.erase(m_id);
            m_parent = nullptr;
        }

    private:
        Emitter* m_parent;
        CallbackId m_id;
    };

    Listener on(EventType type, Callback cb)
    {
        auto id = m_callbacks.insert({ type, cb });
        return Listener(*this, id);
    }

protected:
    Emitter() = default;
    void emit(Event const& evt)
    {
        auto [begin, end] = m_callbacks.equal_range(evt.type());
        for (auto it = begin; it != end; ++it)
            it->second(evt);
    }

    std::unordered_multimap<EventType, Callback> m_callbacks;
};
