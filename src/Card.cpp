#include "Card.h"
#include <fmt/core.h>
#include <math.h>
using namespace std;

std::string Card::sprite_name() const
{
    // TODO: Make card back sprite name configurable
    if (!m_revealed)
        return "cardBack_red5.png";

    std::string suit_name;
    switch (m_suit.value()) {
    case Suit::Clubs:
        suit_name = "Clubs";
        break;
    case Suit::Diamonds:
        suit_name = "Diamonds";
        break;
    case Suit::Hearts:
        suit_name = "Hearts";
        break;
    case Suit::Spades:
        suit_name = "Spades";
        break;
    }
    string rank_name;
    switch (m_rank.value()) {
    case Rank::Ace:
        rank_name = "A";
        break;
    case Rank::Jack:
        rank_name = "J";
        break;
    case Rank::Queen:
        rank_name = "Q";
        break;
    case Rank::King:
        rank_name = "K";
        break;
    default:
        rank_name = to_string(m_rank.value());
    }

    return fmt::format("card{}{}.png", suit_name, rank_name);
}

std::string Card::description() const
{
    std::string suit_name;
    switch (m_suit.value()) {
    case Suit::Clubs:
        suit_name = "Clubs";
        break;
    case Suit::Diamonds:
        suit_name = "Diamonds";
        break;
    case Suit::Hearts:
        suit_name = "Hearts";
        break;
    case Suit::Spades:
        suit_name = "Spades";
        break;
    }

    std::string rank_name;
    switch (m_rank.value()) {
    case Rank::Ace:
        rank_name = "Ace";
        break;
    case Rank::Jack:
        rank_name = "Jack";
        break;
    case Rank::Queen:
        rank_name = "Queen";
        break;
    case Rank::King:
        rank_name = "King";
        break;
    default:
        rank_name = to_string(m_rank.value());
    }

    return fmt::format("{} of {}", rank_name, suit_name);
}

void Card::hide()
{
    if (!m_revealed)
        return;
    m_sprite_dirty = true;
    m_revealed = false;
    m_reveal_animation = AnimationController<float>(1.0f, -1.0f, 0.1f);
}

void Card::reveal()
{
    if (m_revealed)
        return;
    m_sprite_dirty = true;
    m_revealed = true;
    // TODO: Don't animate at the beginning of the game
    m_reveal_animation = AnimationController<float>(1.0f, -1.0f, 0.1f);
}

void Card::teleport_to(glm::vec2 position)
{
    if (m_position == position)
        return;
    m_position = position;
    m_position_dirty = true;
}

void Card::move_to(glm::vec2 position)
{
    if (m_current_animation) {
        m_current_animation->set_target(position);
        return;
    }

    // No need to animate if the target position is very close
    if (glm::length(m_position - position) < 0.0001f) {
        teleport_to(position);
        return;
    }

    m_on_top = true;

    // TODO: Change speed based on distance
    m_current_animation = AnimationController<glm::vec2>(m_position, position, 0.2f);
    m_current_animation->ease();
    m_position_dirty = true;
}

void Card::move_by(glm::vec2 offset) { teleport_to(m_position + offset); }

void Card::move_to_layer(size_t layer)
{
    if (m_layer == layer)
        return;
    m_layer = layer;
    m_position_dirty = true;
}

bool Card::can_add_card_below(Card const& other) const
{
    if (!m_revealed)
        return false;
    if (m_suit.color() == other.suit().color())
        return false;
    return m_rank.value() == other.rank().value() + 1;
}

void Card::update(float elapsed)
{
    if (m_current_animation) {
        m_current_animation->update(elapsed);
        m_position = m_current_animation->value();
        m_position_dirty = true;
        if (m_current_animation->done()) {
            m_current_animation.reset();
            m_on_top = false;
        }
    }

    if (m_reveal_animation) {
        m_reveal_animation->update(elapsed);
        m_x_scale = abs(m_reveal_animation->value());
        // TODO: actually flip the card when we cross 0
        m_position_dirty = true;
        if (m_reveal_animation->done()) {
            m_reveal_animation.reset();
        }
    }
}

float Card::z_index() const
{
    float value = 1.0f - 0.01f * m_layer;
    // If the card is moving, it should be on top of other cards
    if (m_on_top)
        return value - 1.0f;
    return value;
}

void Card::start_dragging()
{
    m_on_top = true;
    m_position_dirty = true;
}

void Card::stop_dragging()
{
    m_on_top = false;
    m_position_dirty = true;
}

int Card::numeric_value() const { return (m_suit.value() - 1) * 13 + m_rank.value() - 1; }
