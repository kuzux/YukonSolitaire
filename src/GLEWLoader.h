#include "Error.h"
#include <string_view>

class GLEWLoader
{
public:
    static ErrorOr<GLEWLoader> create();
    ~GLEWLoader() { }

    bool is_supported(std::string_view extension_name) const;
    void require(std::string_view extension_name);

private:
    GLEWLoader() { }
};
