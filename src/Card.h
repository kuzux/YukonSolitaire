#pragma once

#include <glm/glm.hpp>
#include <optional>
#include <string>

#include "AnimationController.h"

class AbstractPile;

class Card
{
public:
    enum class Color { Red, Black };

    class Suit
    {
    public:
        Suit(int value)
            : m_value(value)
        {
        }
        static constexpr int Hearts = 1;
        static constexpr int Diamonds = 2;
        static constexpr int Clubs = 3;
        static constexpr int Spades = 4;

        int value() const { return m_value; }
        bool operator==(Suit const& other) const { return m_value == other.m_value; }
        bool operator!=(Suit const& other) const { return m_value != other.m_value; }
        Color color() const
        {
            if (m_value == Clubs || m_value == Spades)
                return Color::Black;
            return Color::Red;
        }

    private:
        int m_value;
    };

    class Rank
    {
    public:
        Rank(int value)
            : m_value(value)
        {
        }
        static constexpr int Ace = 1;
        static constexpr int Jack = 11;
        static constexpr int Queen = 12;
        static constexpr int King = 13;

        int value() const { return m_value; }
        bool operator==(Rank const& other) const { return m_value == other.m_value; }
        bool operator!=(Rank const& other) const { return m_value != other.m_value; }

    private:
        int m_value;
    };

    Card(Suit suit, Rank rank)
        : m_suit(suit)
        , m_rank(rank)
    {
    }

    Suit suit() const { return m_suit; }
    Rank rank() const { return m_rank; }
    bool revealed() const { return m_revealed; }
    glm::vec2 position() const { return m_position; }
    float x_scale() const { return m_x_scale; }
    float z_index() const;

    bool highlighted() const { return m_highlighted; }
    void highlight(bool value)
    {
        m_highlighted = value;
        m_position_dirty = true;
    }

    void reveal();
    void hide();

    void move_to(glm::vec2 position);
    void teleport_to(glm::vec2 position);
    void move_by(glm::vec2 offset);

    void move_to_layer(size_t layer);

    bool position_dirty() const { return m_position_dirty; }
    bool sprite_dirty() const { return m_sprite_dirty; }
    void updated_view_model()
    {
        m_position_dirty = false;
        m_sprite_dirty = false;
    }
    int numeric_value() const;

    std::string sprite_name() const;
    std::string description() const;

    bool can_add_card_below(Card const& other) const;

    AbstractPile& pile() const { return *m_pile; }
    void pile(AbstractPile& pile) { m_pile = &pile; }

    void update(float elapsed);
    void start_dragging();
    void stop_dragging();

private:
    glm::vec2 m_position { 1.f, 1.f };
    float m_x_scale { 1.f }; // used in the flipping animation

    Suit m_suit;
    Rank m_rank;
    size_t m_layer { 0 };
    bool m_on_top { false };

    bool m_revealed { false };
    bool m_highlighted { false };

    bool m_position_dirty { true };
    bool m_sprite_dirty { true };
    AbstractPile* m_pile { nullptr };

    std::optional<AnimationController<glm::vec2>> m_current_animation;
    std::optional<AnimationController<float>> m_reveal_animation;
};
