#pragma once
#include <glm/glm.hpp>
#include <vector>

class Card;
#include "Card.h"

#include "AbstractPile.h"
#include "GroupOfCards.h"
class FoundationPile : public AbstractPile
{
public:
    FoundationPile(glm::vec2 position, Card::Suit suit)
        : m_position(position)
        , m_suit(suit)
    {
    }

    void add_card(Card& card);

    bool can_add_group_of_cards(GroupOfCards const& group) const;
    void add_group_of_cards(GroupOfCards const& group);
    void remove_group_of_cards(GroupOfCards const& group);
    GroupOfCards cards_starting_from(Card const& starting_card);

    std::vector<Card*>& cards() { return m_cards; }
    void lay_out_cards();
    glm::vec2 position() const { return m_position; }

    void clear() { m_cards.clear(); }
    size_t size() const { return m_cards.size(); }

private:
    glm::vec2 m_position;
    Card::Suit m_suit;
    std::vector<Card*> m_cards;
};
