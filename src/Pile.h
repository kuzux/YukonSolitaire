#pragma once
#include <glm/glm.hpp>
#include <vector>

#include "AbstractPile.h"
#include "Card.h"
#include "GroupOfCards.h"

class Pile : public AbstractPile
{
public:
    Pile(glm::vec2 topmost_position)
        : m_topmost_position(topmost_position)
    {
    }

    void add_cards(std::vector<Card>& cards, size_t offset, size_t length);

    bool can_add_group_of_cards(GroupOfCards const& group) const;
    void add_group_of_cards(GroupOfCards const& group);
    void remove_group_of_cards(GroupOfCards const& group);

    void reveal_top(size_t N = 1);
    void lay_out_cards();
    bool can_add_cards(Card const& first_card) const;

    GroupOfCards cards_starting_from(Card const& starting_card);
    std::vector<Card*>& cards() { return m_cards; }
    void clear() { m_cards.clear(); }

    glm::vec2 position() const { return m_topmost_position; }

private:
    glm::vec2 m_topmost_position;
    std::vector<Card*> m_cards;
};
