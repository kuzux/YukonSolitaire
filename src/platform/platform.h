#pragma once

#ifdef __cplusplus
#define _Bool bool
enum class PackageType : unsigned char { Executable = 0, OsxApp = 1, AppImage = 2 };

enum class UncertainBool : unsigned char { DefinitelyFalse = 0, DefinitelyTrue = 1, Maybe = 2 };

extern "C" {
#else
typedef enum __attribute__((__packed__)) {
    PACKAGE_TYPE_EXECUTABLE = 0,
    PACKAGE_TYPE_OSX_APP = 1,
    PACKAGE_TYPE_APPIMAGE = 2
} PackageType;

typedef enum __attribute__((__packed__)) {
    DEFINITELY_FALSE = 0,
    DEFINITELY_TRUE = 1,
    MAYBE = 2
} UncertainBool;

#define _Bool signed char
#define _True ((_Bool)1)
#define _False ((_Bool)0)

#endif

// Returns PACKAGE_TYPE_OSX_APP if the app is currently running as an NSApplication
// Returns PACKAGE_TYPE_APPIMAGE if the APPIMAGE env variable is set
// Returns PACKAGE_TYPE_EXECUTABLE otherwise
PackageType application_packaged_as();

// returns true if the currently running main window is visible
// and not obscured by anything else on the screen
UncertainBool is_window_visible();

// tries a path under <BundleDir>/Contents/Resources if the app is running as an NSApplication
// otherwise, just returns a path under assets/
// then it returns NULL if the asset doesn't exist
// the returned string is owned by the caller
const char* get_resource_path(const char* resource);

// if running as NSApplication, returns ~/Library/<AppBundleId>/path
// otherwise, just returns path
const char* get_appdata_path(const char* path);

#ifdef __cplusplus
}
#undef _Bool
#endif
