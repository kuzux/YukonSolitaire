#include "platform.h"

#import <AppKit/AppKit.h>
#import <Foundation/Foundation.h>

#include <unistd.h>
#include <sys/stat.h>

#include <stdio.h>

static bool s_fetched_package = false;
static NSRunningApplication* s_app = nil;

PackageType application_packaged_as()
{
    if (!s_fetched_package) {
        pid_t pid = getpid();
        s_app = [NSRunningApplication runningApplicationWithProcessIdentifier: pid];
        s_fetched_package = true;
    }
    
    if(s_app != nil && [s_app bundleIdentifier] != nil) return PACKAGE_TYPE_OSX_APP;
    return PACKAGE_TYPE_EXECUTABLE;
}

static NSWindow* current_window = nil;

UncertainBool is_window_visible()
{
    if(current_window == nil) {
        // It was necessary to do that to acces the window if it did not have focus
        NSArray<NSWindow*>* windows = [[NSApplication sharedApplication] windows];
        if(!windows) return MAYBE;
        if([windows count] == 0) return MAYBE;

        NSWindow* window = [windows firstObject];
        if(!window) return _False;

        current_window = window;
    }

    NSWindowOcclusionState state = [current_window occlusionState];
    if(state & NSWindowOcclusionStateVisible) return DEFINITELY_TRUE;

    return DEFINITELY_FALSE;
}

const char* get_resource_path(const char* resource) 
{
    const char* c_resource_path = NULL;
    char tmpbuf[1024];

    if (application_packaged_as() == PACKAGE_TYPE_OSX_APP) {
        NSBundle* bundle = [NSBundle mainBundle];
        NSString* bundle_path = [bundle bundlePath];
        NSString* resources_path = [bundle_path stringByAppendingPathComponent: @"Contents/Resources"];

        NSString* resource_name = [NSString stringWithUTF8String: resource];
        NSString* resource_path = [resources_path stringByAppendingPathComponent: resource_name];
        c_resource_path = [resource_path UTF8String];
    } else {
        struct stat st;
        snprintf(tmpbuf, 1024, "dest/%s", resource);
        if (stat(tmpbuf, &st) == 0) {
            c_resource_path = tmpbuf;
        } else {
            snprintf(tmpbuf, 1024, "assets/%s", resource);
            c_resource_path = tmpbuf;
        }
    }

    struct stat statbuf;
    int rc = stat(c_resource_path, &statbuf);

    fprintf(stderr, "original path: %s, resource path: %s, rc=%d\n", resource, c_resource_path, rc);
    
    if (!rc) return strdup(c_resource_path);
    return NULL;
}

const char* get_appdata_path(const char* path)
{
    if (application_packaged_as() != PACKAGE_TYPE_OSX_APP)
        return path;

    NSArray* paths = NSSearchPathForDirectoriesInDomains(NSLibraryDirectory, NSUserDomainMask, YES);
    if ([paths count] == 0)
        return NULL;
    NSString* library_path = [paths objectAtIndex:0];

    NSString* appdata_path = [library_path stringByAppendingPathComponent: [s_app bundleIdentifier]];
    NSFileManager* file_manager = [NSFileManager defaultManager];
    [file_manager createDirectoryAtPath: appdata_path withIntermediateDirectories: YES attributes: nil error: nil];

    NSString* result_path = [appdata_path stringByAppendingPathComponent: [NSString stringWithUTF8String: path]];
    printf("result path in library: %s\n", [result_path UTF8String]);

    return [result_path UTF8String];
}
