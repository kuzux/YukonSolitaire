#include "platform.h"

#include <string.h>
#include <stddef.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

PackageType application_packaged_as()
{
    char* env_var = getenv("APPIMAGE");
    // a non-empty APPIMAGE env var exists
    if(env_var && *env_var) return PACKAGE_TYPE_APPIMAGE;
    return PACKAGE_TYPE_EXECUTABLE;
}

static Window current_window = 0;
static Display* x11_display = 0;

static Window get_window_for_pid(Display* dpy, pid_t pid_to_search)
{
    Window root = DefaultRootWindow(dpy);
    Window found = 0;

    // we iterate through all of the windows first
    // https://stackoverflow.com/a/1211242
    Atom net_client_list = XInternAtom(dpy, "_NET_CLIENT_LIST", 1);
    Atom actual_property_type;

    int format;
    unsigned long num_items;
    unsigned long bytes_after;

    unsigned char* data = 0;
    int status = XGetWindowProperty(dpy, root, net_client_list,
        0L, (~0L), // offset and length, length can be 0xFFFFFFFF
        0, // don't delete the property
        AnyPropertyType,
        &actual_property_type, // the actual type that is returned
        &format,
        &num_items,
        &bytes_after,
        &data);

    if(status < Success || !num_items) return found;
    assert(format == 32);

    // and then find a window that has the target pid
    Window* windows = (Window*)data;

    // https://gist.github.com/tkrajina/7942379 for getting a pid of a window
    Atom net_wm_pid = XInternAtom(dpy, "_NET_WM_PID", 1);
    XTextProperty pid_data;

    for(size_t i=0; i<num_items; i++) {
        Window win = windows[i];
        
        status = XGetTextProperty(dpy, win, &pid_data, net_wm_pid);
        if(pid_data.nitems) {
            pid_t pid = *((pid_t*)pid_data.value);
            if(pid == pid_to_search) {
                found = win;
                break;
            }
        }
    }

    XFree(data);

    return found;
}

static long get_desktop_for_window(Display *display, Window window) {
    Atom prop = XInternAtom(display, "_NET_WM_DESKTOP", False);
    Atom type;
    int format;
    unsigned long nitems, bytes_after;
    unsigned char *data = NULL;
    long desktop = -1;

    if (XGetWindowProperty(display, window, prop, 0, 1, False,
                           AnyPropertyType, &type, &format, &nitems, &bytes_after, &data) == Success) {
        if (data != NULL) {
            desktop = *((long*)data);
            XFree(data);
        }
    }

    return desktop;
}

static long get_active_desktop(Display *display) {
    Atom prop = XInternAtom(display, "_NET_CURRENT_DESKTOP", False);
    Atom type;
    int format;
    unsigned long nitems, bytes_after;
    unsigned char *data = NULL;
    long activeDesktop = -1;

    if (XGetWindowProperty(display, DefaultRootWindow(display), prop, 0, 1, False,
                           AnyPropertyType, &type, &format, &nitems, &bytes_after, &data) == Success) {
        if (data != NULL) {
            activeDesktop = *((long*)data);
            XFree(data);
        }
    }

    return activeDesktop;
}

static UncertainBool is_window_minimized(Display* dpy, Window win)
{
    Atom net_wm_state = XInternAtom(dpy, "_NET_WM_STATE", 1);
    Atom net_wm_state_hidden = XInternAtom(dpy, "_NET_WM_STATE_HIDDEN", 1);
    Atom actual_property_type;

    int format;
    unsigned long num_items;
    unsigned long bytes_after;

    unsigned char* data = 0;
    int status = XGetWindowProperty(dpy, win, net_wm_state,
        0L, (~0L), // offset and length, length can be 0xFFFFFFFF
        0, // don't delete the property
        AnyPropertyType,
        &actual_property_type, // the actual type that is returned
        &format,
        &num_items,
        &bytes_after,
        &data);
    
    if(status != Success) return MAYBE;
    if(!data) return MAYBE;
    if(num_items == 0) return MAYBE;
    Atom* states = (Atom*)data;

    if(states[0] == net_wm_state_hidden) return DEFINITELY_TRUE;

    return DEFINITELY_FALSE;
}

UncertainBool is_window_visible()
{
    if(!x11_display) {
        const char* display_name = getenv("DISPLAY");
        assert(display_name && "DISPLAY env var not set.");
        x11_display = XOpenDisplay(display_name);
    }

    // TODO: Add support for wayland in addition to X11
    if(!x11_display) return MAYBE;

    if(!current_window) current_window = get_window_for_pid(x11_display, getpid());

    // We don't know if the window is visible or not if we can't find it
    if(!current_window) return MAYBE;

    UncertainBool is_minimized = is_window_minimized(x11_display, current_window);
    if(is_minimized == DEFINITELY_TRUE) return DEFINITELY_FALSE;
    if(is_minimized == MAYBE) return MAYBE;
    
    // TODO: This logic might not correctly handle multiple screens
    // Check whether it actually does or does not
    // And then handle multiple screens if necessary
    long desktop_for_window = get_desktop_for_window(x11_display, current_window);
    long active_desktop = get_active_desktop(x11_display);

    if(desktop_for_window != active_desktop) return DEFINITELY_FALSE;

    return DEFINITELY_TRUE;
}

const char* get_resource_path(const char* resource)
{
    char resource_path[1024];

    if(application_packaged_as() == PACKAGE_TYPE_APPIMAGE) {
        char* appdir = getenv("APPDIR");
        assert(appdir);
        snprintf(resource_path, 1024, "%s/usr/share/%s", appdir, resource);

        struct stat stbuf;
        int rc = stat(resource_path, &stbuf);
        if (rc) {
            fprintf(stderr, "Asset missing: %s\n", resource);
            fprintf(stderr, "    Full Path: %s\n", resource_path);
        }
        char* retval = strdup(resource_path);
        assert(retval);
        return retval;
    }
    
    snprintf(resource_path, 1024, "dest/%s", resource);
    struct stat stbuf;
    int rc = stat(resource_path, &stbuf);
    if (!rc) {
        char* retval = strdup(resource_path);
        assert(retval);
        return retval;
    }

    snprintf(resource_path, 1024, "assets/%s", resource);
    rc = stat(resource_path, &stbuf);
    if (rc)
            fprintf(stderr, "Asset missing: %s\n", resource);
    char* retval = strdup(resource_path);
    assert(retval);
    return retval;
}

const char* get_appdata_path(const char* path)
{
    if(application_packaged_as() == PACKAGE_TYPE_EXECUTABLE)
        return path;
    
    char dir_path[1024];
    snprintf(dir_path, 1024, "%s/.yukon", getenv("HOME"));

    struct stat stbuf;
    int rc = stat(dir_path, &stbuf);
    if(rc)
        mkdir(dir_path, 0777);
    // TODO: Create intermediate dirs as well (if necessary)
    
    char data_path[1024];
    rc = snprintf(data_path, 1024, "%s/%s", dir_path, path);
    if(rc == 1024)
        fprintf(stderr, "WARNING: The path %s might be truncated\n", data_path);

    char* retval = strdup(data_path);
    assert(retval);
    return retval;
}
