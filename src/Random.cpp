#include "Random.h"

#include <assert.h>
#include <math.h>
#include <time.h>

namespace Random
{

static Random::State s_seed = 0;
void seed(Random::State seed)
{
    // initial seed must be nonzero
    assert(seed != 0);
    s_seed = seed;
}

uint64_t time_seed()
{
    // TODO: Make this more platform-independent
    struct timespec ts;
    clock_gettime(CLOCK_MONOTONIC, &ts);
    return ts.tv_sec ^ ts.tv_nsec;
}

Random::State seed() { return s_seed; }

static uint64_t xorshift64star(uint64_t* st)
{
    // from https://en.wikipedia.org/wiki/Xorshift
    static uint64_t x = *st;
    x ^= x >> 12;
    x ^= x << 25;
    x ^= x >> 27;
    *st = x;
    return x * 0x2545F4914F6CDD1DULL;
}

int uniform_int()
{
    int res = (int)(xorshift64star(&s_seed));
    return res;
}

// These are the same thing as seed right now, but they could be different
//     (in case the random state was larger)
Random::State raw_rng_state() { return seed(); }
void raw_rng_state(Random::State state) { seed(state); }

int uniform_int(int min, int max)
{
    int r = uniform_int();
    int d = max - min;
    r %= d;
    if (r < 0)
        r += d;
    return min + r;
}

float uniform_float() { return static_cast<float>(uniform_int()) / RAND_MAX; }
float uniform_float(float min, float max) { return min + uniform_float() * (max - min); }

float normal_float()
{
    // box-mueller transform
    float u1 = uniform_float();
    float u2 = uniform_float();

    float r = sqrtf(-2.f * logf(u1));
    r *= cosf(2.f * 3.141592f * u2);
    return r;
}
float normal_float(float mean, float stddev) { return mean + normal_float() * stddev; }

}
