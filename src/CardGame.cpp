#include "CardGame.h"
#include "Persistence.h"
#include "Random.h"
#include "UndoStack.h"

#include <algorithm>
#include <fmt/core.h>

CardGame::CardGame()
{
    assert(!s_the);
    s_the = this;

    m_deck.reserve(52);
    for (int suit = 1; suit <= 4; ++suit)
        for (int rank = 1; rank <= 13; ++rank)
            m_deck.push_back(Card(suit, rank));

    m_piles.reserve(7);
    for (size_t i = 0; i < 7; i++)
        m_piles.emplace_back(Pile { { -.8f + 0.22f * i, .6f } });

    m_foundation_piles.reserve(4);
    for (int i = 0; i < 4; i++) {
        m_foundation_piles.emplace_back(
            FoundationPile { { .8f, -.6f + .4f * i }, static_cast<Card::Suit>(i + 1) });
    }

    assert(m_all_piles.empty());
    assert(m_piles.size() == 7);
    assert(m_foundation_piles.size() == 4);
    for (Pile& pile : m_piles)
        m_all_piles.push_back(&pile);
    for (FoundationPile& foundation_pile : m_foundation_piles)
        m_all_piles.push_back(&foundation_pile);
}

CardGame::~CardGame() { s_the = nullptr; }

void CardGame::start_game()
{
    assert(m_deck.size() == 52);
    assert(m_piles.size() == 7);
    assert(m_foundation_piles.size() == 4);

    for (auto& card : m_deck)
        card.hide();

    size_t cards_offset = 0;
    for (size_t i = 0; i < 7; i++) {
        auto& pile = m_piles[i];
        size_t cards_count = 1;
        if (i > 0)
            cards_count = i + 5;
        pile.clear();
        pile.add_cards(m_deck, cards_offset, cards_count);
        cards_offset += cards_count;
        pile.reveal_top(5);

        pile.lay_out_cards();
    }
    assert(cards_offset == m_deck.size());

    for (auto& pile : m_foundation_piles)
        pile.clear();

    m_number_of_moves = 0;
    m_game_time = 0.f;
    m_game_won = false;
    m_time_after_winning = (float)Random::uniform_int(0, 1000);
    m_highlighted_card = nullptr;

    m_undo_stack.clear();

    emit(GameEvent::Type::Started);
}

void CardGame::new_game()
{
    record_current_game();
    m_game_seed = Random::seed();
    fmt::print("Starting new game with seed: {}\n", m_game_seed);

    // Sorting before shuffle to make sure the shuffles are reproducible later
    std::sort(m_deck.begin(), m_deck.end(),
        [](Card const& a, Card const& b) { return a.numeric_value() < b.numeric_value(); });
    Random::shuffle(m_deck);
    start_game();
}

void CardGame::reset_game()
{
    record_current_game();
    fmt::print("Resetting game\n");
    start_game();
}

void CardGame::record_current_game()
{
    if (m_game_time < 1.0f)
        return;
    if (m_game_seed == 0)
        return;
    if (m_number_of_moves == 0)
        return;

    Persistence::the().add_game_data(m_game_seed, (int)m_game_time, m_number_of_moves, m_game_won);
}

void CardGame::tick_time(float delta_seconds)
{
    if (m_game_won)
        m_time_after_winning += delta_seconds;
    else
        m_game_time += delta_seconds;

    for (auto& card : m_deck)
        card.update(delta_seconds);
}

void CardGame::mark_as_won()
{
    m_game_won = true;
    emit(GameEvent::Type::GameWon);
    fmt::print("Game won!\n");
}

bool CardGame::can_auto_win() const
{
    for (auto const& card : m_deck) {
        if (!card.revealed())
            return false;
    }
    return true;
}

void CardGame::auto_win()
{
    if (!can_auto_win()) {
        fmt::print("No cheating, not all cards are revealed");
        return;
    }
    mark_as_won();
}

void CardGame::mouse_down(glm::vec2 const& position)
{
    auto* found_card = find_card_at(position);
    if (!found_card)
        return;

    if (!found_card->revealed())
        return;

    m_dragged_cards
        = std::make_unique<GroupOfCards>(found_card->pile().cards_starting_from(*found_card));
    m_dragged_cards->start_dragging();
    m_drag_start_position = position;
    m_current_drag_position = position;
}

void CardGame::mouse_moved_to(glm::vec2 const& position)
{
    auto* found_card = find_card_at(position);

    if (m_highlighted_card != found_card) {
        if (m_highlighted_card)
            m_highlighted_card->highlight(false);
        if (found_card)
            found_card->highlight(true);
        m_highlighted_card = found_card;
    }

    if (m_dragged_cards) {
        auto delta = position - m_current_drag_position;
        m_dragged_cards->move_by(delta);
        m_current_drag_position = position;
    }
}

void CardGame::mouse_clicked(glm::vec2 const& position)
{
    if (m_dragged_cards) {
        m_dragged_cards->cancel_drag();
        m_dragged_cards->source_pile().lay_out_cards();
        m_dragged_cards.reset();
    }

    auto* found_card = find_card_at(position);
    if (!found_card)
        return;
    if (!found_card->revealed())
        return;

    fmt::print("Clicked on card: {}\n", found_card->description());

    auto& src_pile = found_card->pile();
    auto cards = src_pile.cards_starting_from(*found_card);

    auto* dest_pile = find_pile_for_cards(cards);
    if (!dest_pile)
        return;

    assert(dest_pile->can_add_group_of_cards(cards));

    cards.move_to(*dest_pile);
    m_undo_stack.add_action(cards);
    m_number_of_moves++;

    emit(GameEvent::Type::CardMoved);

    dest_pile->lay_out_cards();
    src_pile.lay_out_cards();

    if (check_game_won())
        mark_as_won();
}

void CardGame::mouse_drag_end(glm::vec2 const& position)
{
    if (!m_dragged_cards)
        return;

    auto* found_card = find_card_at(position);
    auto* found_pile = find_empty_pile_at(position);
    if (!found_card && !found_pile) {
        m_dragged_cards->cancel_drag();
        m_dragged_cards->source_pile().lay_out_cards();
        m_dragged_cards.reset();
        return;
    }

    auto& target_pile = found_card ? found_card->pile() : *found_pile;

    bool dropped_on_same_pile = &target_pile == &m_dragged_cards->source_pile();
    bool cant_drop_on_pile = !target_pile.can_add_group_of_cards(*m_dragged_cards);
    if (dropped_on_same_pile || cant_drop_on_pile) {
        m_dragged_cards->cancel_drag();
        m_dragged_cards->source_pile().lay_out_cards();
        m_dragged_cards.reset();
        return;
    }

    m_dragged_cards->drop_on_pile(target_pile);
    m_undo_stack.add_action(*m_dragged_cards);
    m_number_of_moves++;

    emit(GameEvent::Type::CardMoved);

    m_dragged_cards->source_pile().lay_out_cards();
    target_pile.lay_out_cards();

    m_dragged_cards.reset();
    m_highlighted_card = nullptr;
}

void CardGame::mouse_up(glm::vec2 const& position)
{
    auto length = glm::length(position - m_drag_start_position);
    if (length < 0.01f)
        mouse_clicked(position);
    else
        mouse_drag_end(position);
}

AbstractPile* CardGame::find_pile_for_cards(GroupOfCards const& cards) const
{
    int found_index = -1;
    for (size_t i = 0; i < m_all_piles.size(); i++) {
        if (m_all_piles[i] == &cards.source_pile()) {
            found_index = i;
            break;
        }
    }
    for (size_t i = 1; i <= m_all_piles.size(); i++) {
        size_t idx = (found_index + i) % m_all_piles.size();
        auto* pile = m_all_piles[idx];
        if (pile->can_add_group_of_cards(cards))
            return pile;
    }
    return nullptr;
}

Card* CardGame::find_card_at(glm::vec2 const& position)
{
    Card* found_card = nullptr;
    if (m_deck.empty())
        return nullptr;

    float found_z_index = 100.0f;

    for (Card& card : m_deck) {
        if (position.x < card.position().x - m_card_scale.x)
            continue;
        if (position.x > card.position().x + m_card_scale.x)
            continue;
        if (position.y < card.position().y - m_card_scale.y)
            continue;
        if (position.y > card.position().y + m_card_scale.y)
            continue;

        // Ignore the card currently being dragged
        if (m_dragged_cards && &card == &m_dragged_cards->first_card())
            continue;

        if (card.z_index() > found_z_index)
            continue;
        found_card = &card;
        found_z_index = card.z_index();
    }

    return found_card;
}

AbstractPile* CardGame::find_empty_pile_at(glm::vec2 const& position)
{
    for (auto* pile : m_all_piles) {
        if (!pile->cards().empty())
            continue;
        if (position.x < pile->position().x - m_card_scale.x)
            continue;
        if (position.x > pile->position().x + m_card_scale.x)
            continue;
        if (position.y < pile->position().y - m_card_scale.y)
            continue;
        if (position.y > pile->position().y + m_card_scale.y)
            continue;

        return pile;
    }
    return nullptr;
}

bool CardGame::check_game_won() const
{
    for (auto const& pile : m_foundation_piles) {
        if (pile.size() != 13)
            return false;
    }
    return true;
}

void CardGame::undo()
{
    if (!m_undo_stack.can_undo())
        return;
    auto& undo_action = m_undo_stack.action_to_undo();
    undo_action.undo_move();
    undo_action.source_pile().lay_out_cards();
    undo_action.destination_pile().lay_out_cards();
}

void CardGame::redo()
{
    if (!m_undo_stack.can_redo())
        return;
    auto& undo_action = m_undo_stack.action_to_redo();
    undo_action.redo_move();
    undo_action.source_pile().lay_out_cards();
    undo_action.destination_pile().lay_out_cards();
}
