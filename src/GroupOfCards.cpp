#include "GroupOfCards.h"
#include "Pile.h"

Card& GroupOfCards::first_card() const
{
    if (m_done)
        return *m_destination_pile->cards()[m_destination_offset];
    return *m_source_pile.cards()[m_source_offset];
}

void GroupOfCards::move_to(AbstractPile& destination_pile)
{
    m_destination_pile = &destination_pile;
    m_destination_offset = m_destination_pile->cards().size();
    if (&m_source_pile != &destination_pile) {
        destination_pile.add_group_of_cards(*this);
        m_source_pile.remove_group_of_cards(*this);
    }
    m_done = true;
}

void GroupOfCards::move_by(glm::vec2 const& offset)
{
    assert(!m_done);
    auto& cards = m_source_pile.cards();
    for (size_t i = 0; i < m_length; i++)
        cards[m_source_offset + i]->move_by(offset);
}

void GroupOfCards::undo_move()
{
    assert(m_destination_pile);
    if (&m_source_pile != m_destination_pile) {
        m_source_pile.add_group_of_cards(*this);
        m_destination_pile->remove_group_of_cards(*this);
    }
    m_done = false;
}

void GroupOfCards::redo_move()
{
    assert(m_destination_pile);
    if (&m_source_pile != m_destination_pile) {
        m_destination_pile->add_group_of_cards(*this);
        m_source_pile.remove_group_of_cards(*this);
    }
    m_done = true;
}

bool GroupOfCards::revealed_a_card() const { return m_will_reveal_card && m_done; }

void GroupOfCards::start_dragging()
{
    assert(!m_done);
    for (size_t i = 0; i < m_length; i++)
        m_source_pile.cards()[m_source_offset + i]->start_dragging();
}

void GroupOfCards::cancel_drag()
{
    assert(!m_done);
    for (size_t i = 0; i < m_length; i++)
        m_source_pile.cards()[m_source_offset + i]->stop_dragging();
}

void GroupOfCards::drop_on_pile(AbstractPile& pile)
{
    assert(!m_done);
    for (size_t i = 0; i < m_length; i++)
        m_source_pile.cards()[m_source_offset + i]->stop_dragging();
    m_destination_pile = &pile;
    m_destination_offset = pile.cards().size();
    m_destination_pile->add_group_of_cards(*this);
    m_source_pile.remove_group_of_cards(*this);
}
