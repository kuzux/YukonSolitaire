#pragma once

#include <array>
#include <cassert>
#include <functional>
#include <vector>

#include "Error.h"
#include "GLTypes.h"

enum class GLBufferTarget { ArrayBuffer, ElementArrayBuffer };

ErrorOr<void> _gl_buffer_data(GLBufferTarget target, size_t bytes, void const* data);

// returns the glid of the formerly bound buffer
GL::uint _gl_bind_buffer(GL::uint glid, GLBufferTarget target);

GL::uint _gl_arraybuffer_create();
void _gl_arraybuffer_cleanup(GL::uint glid);

template <typename T> class GLArrayBuffer
{
public:
    struct Binding {
        friend class GLArrayBuffer;
        Binding(GLArrayBuffer& vbo)
            : m_vbo(vbo)
        {
            assert(!m_vbo.m_is_bound);
            m_old_glid = _gl_bind_buffer(m_vbo.m_glid, m_vbo.m_target);
            m_vbo.m_is_bound = true;
        }
        ~Binding()
        {
            assert(m_vbo.m_is_bound);
            _gl_bind_buffer(m_old_glid, m_vbo.m_target);
            m_vbo.m_is_bound = false;
        }

    private:
        GLArrayBuffer& m_vbo;
        GL::uint m_old_glid { 0 };
    };

    GLArrayBuffer(GLBufferTarget target)
    {
        m_target = target;
        m_glid = _gl_arraybuffer_create();
    }
    GLArrayBuffer(GLArrayBuffer<T>&& o)
        : m_glid(o.m_glid)
    {
        // Make sure the other buffer is not being used at the moment
        assert(!o.m_is_bound);

        o.m_glid = 0;
    }
    ~GLArrayBuffer()
    {
        assert(!m_is_bound);

        if (m_glid)
            _gl_arraybuffer_cleanup(m_glid);
    }

    [[nodiscard]] Binding bind() { return Binding(*this); }

    template <size_t N> ErrorOr<void> load(std::array<T, N> data)
    {
        assert(m_glid);
        assert(m_is_bound);

        m_size = N;
        return _gl_buffer_data(m_target, N * sizeof(T), data.data());
    }

    ErrorOr<void> load(std::vector<T> data)
    {
        assert(m_glid);
        assert(m_is_bound);

        m_size = data.size();
        return _gl_buffer_data(m_target, data.size() * sizeof(T), data.data());
    }

    size_t size() const { return m_size; }

private:
    GL::uint m_glid { 0 };
    GLBufferTarget m_target;
    bool m_is_bound { false };
    size_t m_size { 0 };
};
