#pragma once

#include <memory>
#include <vector>

#include "Scene.h"

class SceneStack
{
public:
    static SceneStack& the() { return *s_the; }
    SceneStack();
    ~SceneStack() = default;

    SceneStack& operator=(SceneStack const&) = delete;
    SceneStack(SceneStack const&) = delete;

    Scene& current_scene() { return *m_scenes.back(); }

    template <typename SceneType, typename... Args> void push(Args&&... args)
    {
        auto ptr = std::make_unique<SceneType>(std::forward<Args>(args)...);
        m_scenes.emplace_back(std::move(ptr));
        attach_new_scene();
    }
    void pop();

private:
    void attach_new_scene();
    inline static SceneStack* s_the { nullptr };
    std::vector<std::unique_ptr<Scene>> m_scenes;
};
