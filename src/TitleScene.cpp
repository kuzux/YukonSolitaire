#include "TitleScene.h"

#include <array>
#include <tuple>

#include "GLArrayBuffer.h"
#include "GLFramework.h"
#include "GLShader.h"
#include "GLTexture.h"
#include "GLVertexArray.h"
#include "platform/platform.h"

#include "GameScene.h"
#include "Persistence.h"
#include "Random.h"
#include "SceneStack.h"

#include "vendor/imgui.h"

using namespace std;

// TODO: VertexInfo and s_vertex_buffer_data should be moved to a separate file
//        since they are used in both game and title scenes
struct VertexInfo {
    glm::vec2 pos;
    glm::vec2 uv;
};

// clang-format off
static constexpr array<VertexInfo, 6> s_vertex_buffer_data = {{ 
    // vertex position         texture coords
    { { -1.f, -1.f },         { 0.f, 1.f } },
    { { 1.f,  -1.f },         { 1.f, 1.f } },
    { { -1.f,  1.f },         { 0.f, 0.f } },

    { { -1.f,  1.f },         { 0.f, 0.f } }, 
    { { 1.f,  -1.f },         { 1.f, 1.f } },
    { { 1.f,   1.f },         { 1.f, 0.f } } 
}};
// clang-format on

struct TitleScene::Impl {
    GLVertexArray vao;
    GLArrayBuffer<VertexInfo> vbo { GLBufferTarget::ArrayBuffer };
    GLProgram program;
    GLTexture texture { { 1024, 742 } };
    Emitter<SDLEvent>::Listener on_quit;

    char seed_string[32];
};

TitleScene::TitleScene()
    : m_impl(make_unique<Impl>())
{
    {
        auto vao_binding = impl().vao.bind();
        auto vbo_binding = impl().vbo.bind();
        impl().vbo.load(s_vertex_buffer_data);
        impl().vao.define_attribute(0, GLVertexAttribute(glm::vec2, VertexInfo, pos));
        impl().vao.define_attribute(1, GLVertexAttribute(glm::vec2, VertexInfo, uv));
    }
    {
        auto vertex_shader = MUST(
            GLShader::from_file(get_resource_path("title.vert.glsl"), GLShader::Type::Vertex));
        auto fragment_shader = MUST(
            GLShader::from_file(get_resource_path("title.frag.glsl"), GLShader::Type::Fragment));

        impl().program.attach(vertex_shader);
        impl().program.attach(fragment_shader);
        MUST(impl().program.link());
    }
    {
        auto binding = impl().texture.bind(GL_TEXTURE0);
        impl().texture.load_from_file(get_resource_path("title_image.png"));
    }
    {
        auto program_binding = impl().program.bind();
        impl().program.get_uniform<glm::vec2>("position").set_value({ 0.f, .4f });
        impl().program.get_uniform<glm::vec2>("scale").set_value({ 0.55f, 0.4f });
    }

    load_settings();
}

TitleScene::~TitleScene() = default;

void TitleScene::attach_to(SDLEventLoop& event_loop)
{
    impl().on_quit = event_loop.on(SDL_QUIT, [&](SDLEvent const&) { save_settings(); });
}

void TitleScene::detach()
{
    impl().on_quit.off();
    save_settings();
}

void TitleScene::draw(SDLFrameInfo const& frame)
{
    (void)frame;

    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    auto vao_binding = impl().vao.bind();
    auto program_binding = impl().program.bind();
    auto texture_binding = impl().texture.bind(GL_TEXTURE0);

    glDrawArrays(GL_TRIANGLES, 0, 6);
}

static bool ButtonCenteredOnLine(const char* label, float alignment = 0.5f)
{
    ImGuiStyle& style = ImGui::GetStyle();

    float size = ImGui::CalcTextSize(label).x + style.FramePadding.x * 2.0f;
    float avail = ImGui::GetContentRegionAvail().x;

    float off = (avail - size) * alignment;
    if (off > 0.0f)
        ImGui::SetCursorPosX(ImGui::GetCursorPosX() + off);

    return ImGui::Button(label);
}

static bool CheckboxCenteredOnLine(const char* label, bool* value, float alignment = 0.5f)
{
    ImGuiStyle& style = ImGui::GetStyle();

    float size = ImGui::CalcTextSize(label).x + style.FramePadding.x * 2.0f;
    float avail = ImGui::GetContentRegionAvail().x;

    float off = (avail - size) * alignment;
    if (off > 0.0f)
        ImGui::SetCursorPosX(ImGui::GetCursorPosX() + off);

    return ImGui::Checkbox(label, value);
}

static bool SliderCenteredOnLine(
    const char* label, float* value, float min, float max, float slider_width = 0.5f)
{
    auto& style = ImGui::GetStyle();
    auto slider_size = ImGui::GetContentRegionAvail().x * slider_width;
    ImGui::SetNextItemWidth(slider_size);
    auto total_size = slider_size + style.FramePadding.x + ImGui::CalcTextSize(label).x;
    auto avail = ImGui::GetContentRegionAvail().x;

    float off = (avail - total_size) * 0.5;
    if (off > 0.0f)
        ImGui::SetCursorPosX(ImGui::GetCursorPosX() + off);

    return ImGui::SliderFloat(label, value, min, max);
}

template <typename... Args> static void LabelCenteredOnLine(std::string const& fmt, Args... args)
{
    auto msg = fmt::format(fmt::runtime(fmt), args...);
    float size = ImGui::CalcTextSize(msg.c_str()).x;
    float avail = ImGui::GetContentRegionAvail().x;

    float off = (avail - size) * 0.5;
    if (off > 0.0f)
        ImGui::SetCursorPosX(ImGui::GetCursorPosX() + off);

    ImGui::Text("%s", msg.c_str());
}

void TitleScene::draw_ui()
{
    auto size = SDLWindow::the().window_size();
    ImGui::SetNextWindowSize({ 300.f, 250.f });
    ImGui::SetNextWindowPos({ size.x * 0.5f - 150.f, size.y * 0.6f });
    ImGui::Begin("Yukon Solitaire", nullptr,
        ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoCollapse);
    ImGui::Dummy({ 0.f, 10.f });
    if (m_show_high_scores)
        draw_high_scores();
    else if (m_show_stats)
        draw_stats();
    else if (m_show_seed_input)
        draw_seed_input();
    else if (m_show_settings)
        draw_settings();
    else
        draw_main_menu();
    ImGui::End();
}

void TitleScene::draw_main_menu()
{
    if (ButtonCenteredOnLine("Start Game"))
        SceneStack::the().push<GameScene>();
    ImGui::Dummy({ 0.f, 5.f });
    if (ButtonCenteredOnLine("Start Game with Seed"))
        m_show_seed_input = true;
    ImGui::Dummy({ 0.f, 5.f });
    if (ButtonCenteredOnLine("High Scores"))
        m_show_high_scores = true;
    ImGui::Dummy({ 0.f, 5.f });
    if (ButtonCenteredOnLine("Game Stats"))
        m_show_stats = true;
    ImGui::Dummy({ 0.f, 5.f });
    if (ButtonCenteredOnLine("Settings"))
        m_show_settings = true;
    ImGui::Dummy({ 0.f, 5.f });
    if (ButtonCenteredOnLine("Quit")) {
        save_settings();
        SDLEventLoop::the().stop();
    }
}

void TitleScene::draw_high_scores()
{
    ImGui::SetWindowFontScale(1.5f);
    LabelCenteredOnLine("High Scores");
    ImGui::SetWindowFontScale(1.f);

    std::vector<int> shortest_times = Persistence::the().shortest_times(5);

    ImGui::Dummy({ 0.f, 5.f });
    for (size_t i = 0; i < shortest_times.size(); i++) {
        auto mins = shortest_times[i] / 60;
        auto secs = shortest_times[i] % 60;
        LabelCenteredOnLine("{}. {:02d}:{:02d}", i + 1, mins, secs);
    }
    ImGui::Dummy({ 0.f, 10.f });
    if (ButtonCenteredOnLine("Back"))
        m_show_high_scores = false;
}

void TitleScene::draw_stats()
{
    ImGui::SetWindowFontScale(1.5f);
    LabelCenteredOnLine("Game Stats");
    ImGui::SetWindowFontScale(1.f);

    auto stats = Persistence::the().get_stats();
    auto games_played = stats.games_played;
    auto games_won = stats.games_won;
    auto win_rate = stats.win_ratio;
    auto time_to_win = stats.avg_winning_time;
    auto play_time = stats.seconds_played;

    LabelCenteredOnLine("Games Played: {}", games_played);
    LabelCenteredOnLine("Games Won: {}", games_won);
    LabelCenteredOnLine("Win Rate: {:2.02f}%", win_rate * 100.);
    LabelCenteredOnLine(
        "Average Time to Win: {:02d}:{:02d} minutes", (int)time_to_win / 60, (int)time_to_win % 60);
    LabelCenteredOnLine("Total Play Time: {:02d}:{:02d}:{:02d}", (int)play_time / 3600,
        (int)play_time / 60, (int)play_time % 60);

    ImGui::Dummy({ 0.f, 10.f });
    if (ButtonCenteredOnLine("Back"))
        m_show_stats = false;
}

void TitleScene::draw_seed_input()
{
    ImGui::InputText("Seed", impl().seed_string, 32);
    if (ButtonCenteredOnLine("Start Game")) {
        uint64_t seed = strtoull(impl().seed_string, nullptr, 10);
        Random::seed(seed);
        SceneStack::the().push<GameScene>();
    }
    if (ButtonCenteredOnLine("Back"))
        m_show_seed_input = false;
}

void TitleScene::draw_settings()
{
    ImGui::SetWindowFontScale(1.5f);
    LabelCenteredOnLine("Settings");
    ImGui::SetWindowFontScale(1.f);

    ImGui::Dummy({ 0.f, 5.f });

    float volume = SDLWindow::the().audio_player().music_volume();
    bool volume_changed = SliderCenteredOnLine("Music Volume", &volume, 0.f, 1.f);
    if (volume_changed)
        SDLWindow::the().audio_player().music_volume(volume);

    ImGui::Dummy({ 0.f, 5.f });

    float sfx_volume = SDLWindow::the().audio_player().sfx_volume();
    bool sfx_volume_changed = SliderCenteredOnLine("SFX Volume", &sfx_volume, 0.f, 1.f);
    if (sfx_volume_changed)
        SDLWindow::the().audio_player().sfx_volume(sfx_volume);

    ImGui::Dummy({ 0.f, 5.f });
    bool fullscreen = SDLWindow::the().fullscreen();
    bool fullscreen_changed = CheckboxCenteredOnLine("Fullscreen", &fullscreen);
    if (fullscreen_changed)
        SDLWindow::the().fullscreen(fullscreen);

    ImGui::Dummy({ 0.f, 5.f });
    if (ButtonCenteredOnLine("Back"))
        m_show_settings = false;
}

void TitleScene::save_settings()
{
    auto& audio_player = SDLWindow::the().audio_player();
    Persistence::the().save_setting("music_volume", audio_player.music_volume());
    Persistence::the().save_setting("sfx_volume", audio_player.sfx_volume());
    Persistence::the().save_setting("fullscreen", SDLWindow::the().fullscreen());
}

void TitleScene::load_settings()
{
    auto music_volume = Persistence::the().load_setting<float>("music_volume");
    if (music_volume.is_error()) {
        fmt::print("Setting music volume to default\n");
        SDLWindow::the().audio_player().music_volume(0.5f);
    } else {
        SDLWindow::the().audio_player().music_volume(music_volume.release_value());
    }

    auto sfx_volume = Persistence::the().load_setting<float>("sfx_volume");
    if (sfx_volume.is_error()) {
        fmt::print("Setting sfx volume to default\n");
        SDLWindow::the().audio_player().sfx_volume(0.5f);
    } else {
        SDLWindow::the().audio_player().sfx_volume(sfx_volume.release_value());
    }

    auto fullscreen = Persistence::the().load_setting<int>("fullscreen");
    if (fullscreen.is_error()) {
        fmt::print("Setting fullscreen to default\n");
        SDLWindow::the().fullscreen(false);
    } else {
        SDLWindow::the().fullscreen(fullscreen.release_value());
    }
}
