#pragma once

#include <array>
#include <stdint.h>
#include <utility>
#include <vector>

namespace Random
{
using State = uint64_t;

void seed(uint64_t seed);
uint64_t seed();
uint64_t time_seed();

// Save and restore the state of the RNG
void raw_rng_state(State state);
State raw_rng_state();

int uniform_int();
int uniform_int(int min, int max);

float uniform_float();
float uniform_float(float min, float max);

float normal_float();
float normal_float(float mean, float stddev);

// TODO: Add other distributions as well

template <typename T> void shuffle(std::vector<T>& vec)
{
    for (size_t i = vec.size() - 1; i > 0; --i) {
        size_t j = uniform_int(0, i);
        std::swap(vec[i], vec[j]);
    }
}

template <typename T, size_t N> void shuffle(std::array<T, N>& arr)
{
    for (size_t i = N - 1; i > 0; --i) {
        size_t j = uniform_int(0, i);
        std::swap(arr[i], arr[j]);
    }
}

}
