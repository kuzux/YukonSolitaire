#pragma once

#include "GroupOfCards.h"
#include <vector>

class UndoStack
{
public:
    void add_action(GroupOfCards const& group);

    bool can_undo() const { return m_cursor > 0; }
    GroupOfCards& action_to_undo();

    bool can_redo() const { return m_cursor < m_actions.size(); }
    GroupOfCards& action_to_redo();

    void clear();

private:
    std::vector<GroupOfCards> m_actions;
    size_t m_cursor { 0 };
};
