#include "UndoStack.h"

void UndoStack::add_action(GroupOfCards const& group)
{
    assert(group.has_destination());
    while (m_cursor > m_actions.size())
        m_actions.pop_back();
    m_actions.push_back(group);
    m_cursor = m_actions.size();
}

GroupOfCards& UndoStack::action_to_undo()
{
    assert(m_cursor <= m_actions.size());
    assert(m_cursor > 0);
    m_cursor--;
    return m_actions[m_cursor];
}

GroupOfCards& UndoStack::action_to_redo()
{
    assert(m_cursor < m_actions.size());
    m_cursor++;
    return m_actions[m_cursor - 1];
}

void UndoStack::clear()
{
    m_actions.clear();
    m_cursor = 0;
}
