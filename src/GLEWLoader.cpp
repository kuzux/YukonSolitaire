#include "GLEWLoader.h"
#include "GLFramework.h"
#include <assert.h>
#include <string>
using namespace std;

ErrorOr<GLEWLoader> GLEWLoader::create()
{
    auto rc = glewInit();
    if (rc != GLEW_OK) {
        string msg = reinterpret_cast<const char*>(glewGetErrorString(rc));
        return Error::with_message(fmt::format("Error loading glew: {}", msg));
    }
    return GLEWLoader();
}

bool GLEWLoader::is_supported(string_view extension_name) const
{
    string copy(extension_name);
    return glewIsSupported(copy.c_str());
}

void GLEWLoader::require(string_view extension_name) { assert(is_supported(extension_name)); }
