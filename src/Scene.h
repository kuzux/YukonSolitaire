#pragma once

#include "SDLWindow.h"

class Scene
{
public:
    virtual void attach_to(SDLEventLoop& event_loop) = 0;
    virtual void detach() = 0;

    virtual void draw(SDLFrameInfo const& frame) = 0;
    virtual void draw_ui() = 0;

    virtual ~Scene() = default;

    virtual void draw_secondary(SDLFrameInfo const& frame) { draw(frame); }
};
