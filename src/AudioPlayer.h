#pragma once

#include "Error.h"
#include <SDL_audio.h>
#include <string>

#include <deque>
#include <mutex>
#include <vector>

class Sound
{
    friend class AudioPlayer;

public:
    enum class Type : uint8_t { Music, Sfx };

    Sound(Sound&&);
    Sound& operator=(Sound&&);
    ~Sound();

    static ErrorOr<Sound> load(std::string const& pat, Type type);
    bool loaded() const { return m_buffer != nullptr; }
    size_t num_samples() const { return m_length / (m_spec.channels * sizeof(float)); }
    int length_in_ms() const { return num_samples() / (m_spec.freq / 1000); }

    SDL_AudioSpec const& spec() const { return m_spec; }

    // TODO: play, pause, stop, loop, volume, etc.
private:
    Sound() = default;

    SDL_AudioSpec m_spec;
    std::string m_path;

    // Might be loaded by sdl
    uint8_t* m_buffer { nullptr };
    uint32_t m_length { 0 }; // in bytes
    bool m_from_sdl { true };
    Type m_type;
};

class AudioPlayer
{
    class PlayingSound
    {
    public:
        PlayingSound(Sound const& sound, AudioPlayer& player, bool looping);
        ~PlayingSound();

        PlayingSound(PlayingSound&& other);
        PlayingSound& operator=(PlayingSound&&);

        void reset();
        void stop() { m_stopped = true; }
        bool finished() const;
        size_t available_resampled_bytes() { return SDL_AudioStreamAvailable(m_stream); }

        SDL_AudioStream* stream() { return m_stream; }
        bool looping() const { return m_looping; }
        Sound::Type type() const { return m_sound->m_type; }
        Sound const& sound() const { return *m_sound; }

        void advance_cursor(size_t samples) { m_play_cursor += samples; }

        void resample_more();

    private:
        Sound const* m_sound;
        SDL_AudioStream* m_stream;

        size_t m_play_cursor { 0 }; // in number of samples
        size_t m_resample_cursor { 0 }; // in bytes
        bool m_looping { false };
        bool m_stopped { false };
        bool m_has_stream { false };
    };

    struct QueuedSound {
        Sound const* sound;
        size_t starting_offset { 0 };
        bool looping { false };
        QueuedSound(Sound const& sound, size_t offset, bool looping)
            : sound(&sound)
            , starting_offset(offset)
            , looping(looping)
        {
        }
    };

public:
    AudioPlayer() = default;

    // TODO: select/change audio device
    ErrorOr<void> initialize();
    bool initialized() const { return m_device_id != 0; }

    ErrorOr<void> play_after(Sound const& sound, size_t ms, bool loop);
    ErrorOr<void> play(Sound const& sound) { return play_after(sound, 0, false); }
    ErrorOr<void> play_looping(Sound const& sound) { return play_after(sound, 0, true); }

    void music_volume(float volume) { m_music_volume = volume; }
    float music_volume() const { return m_music_volume; }
    void sfx_volume(float volume) { m_sfx_volume = volume; }
    float sfx_volume() const { return m_sfx_volume; }

    void pause() { m_paused = true; }
    void resume() { m_paused = false; }

    void stop_all();
    void stop(Sound const& sound);

private:
    static void sdl_audio_callback(void* userdata, uint8_t* stream, int len);
    size_t ms_to_frame_count(size_t ms) const { return ms * m_spec.freq / 1000; }
    size_t bytes_per_frame() const { return m_spec.channels * sizeof(float); }

    SDL_AudioDeviceID m_device_id { 0 };
    SDL_AudioSpec m_spec;

    std::deque<QueuedSound> m_sound_queue;
    std::vector<PlayingSound> m_playing_sounds;
    size_t m_playback_offset { 0 }; // in sample frames

    std::mutex m_sound_queue_mutex;

    float m_music_volume { 1.0f };
    float m_sfx_volume { 1.0f };
    bool m_paused { false };
};
