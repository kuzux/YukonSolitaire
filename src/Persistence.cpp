#include "Persistence.h"
#include <assert.h>
#include <fmt/core.h>
#include <sqlite3.h>
using namespace std;

Persistence& Persistence::the()
{
    if (!s_the)
        s_the = new Persistence();
    return *s_the;
}

ErrorOr<void> Persistence::load_database(string const& path)
{
    int rc = sqlite3_open_v2(
        path.c_str(), (sqlite3**)&m_db, SQLITE_OPEN_CREATE | SQLITE_OPEN_READWRITE, nullptr);
    if (rc != SQLITE_OK) {
        m_db = nullptr;
        return Error::with_message("Could not open database: {}", sqlite3_errstr(rc));
    }

    TRY(create_schema());
    return {};
}

ErrorOr<void> Persistence::create_schema()
{
    const char* sql = "CREATE TABLE IF NOT EXISTS games ("
                      "id INTEGER PRIMARY KEY AUTOINCREMENT,"
                      "seed UNSIGNED BIGINT NOT NULL,"
                      "timestamp DATETIME DEFAULT CURRENT_TIMESTAMP,"
                      "duration INTEGER NOT NULL,"
                      "moves INTEGER NOT NULL,"
                      "won BOOLEAN NOT NULL"
                      ");"
                      "CREATE TABLE IF NOT EXISTS settings ("
                      "key TEXT NOT NULL UNIQUE,"
                      "int_value INTEGER,"
                      "float_value REAL,"
                      "string_value TEXT"
                      ");";
    char* err_msg = nullptr;
    int rc = sqlite3_exec((sqlite3*)m_db, sql, nullptr, nullptr, &err_msg);
    if (rc != SQLITE_OK) {
        return Error::with_message("Could not create tables: {}", err_msg);
    }
    return {};
}

Persistence::~Persistence()
{
    if (m_db)
        sqlite3_close((sqlite3*)m_db);
}

void Persistence::add_game_data(
    uint64_t game_seed, int seconds_played, int number_of_moves, bool won)
{
    assert(m_db);
    const char* sql = "INSERT INTO games (seed, duration, moves, won) VALUES (?, ?, ?, ?);";
    sqlite3_stmt* stmt;
    int rc = sqlite3_prepare_v2((sqlite3*)m_db, sql, -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        fmt::print("Could not prepare statement: {}", sqlite3_errmsg((sqlite3*)m_db));
        assert(false);
    }

    sqlite3_bind_int64(stmt, 1, game_seed); // We can pretend the seed is a signed int to sqlite
    sqlite3_bind_int(stmt, 2, seconds_played);
    sqlite3_bind_int(stmt, 3, number_of_moves);
    sqlite3_bind_int(stmt, 4, won);
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE) {
        fmt::print("Could not execute statement: {}", sqlite3_errmsg((sqlite3*)m_db));
        assert(false);
    }

    sqlite3_finalize(stmt);
}

vector<int> Persistence::shortest_times(size_t count)
{
    assert(m_db);
    const char* sql = "SELECT duration FROM games WHERE won = 1 ORDER BY duration ASC LIMIT ?;";
    sqlite3_stmt* stmt;
    int rc = sqlite3_prepare_v2((sqlite3*)m_db, sql, -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        fmt::print("Could not prepare statement: {}", sqlite3_errmsg((sqlite3*)m_db));
        assert(false);
    }

    sqlite3_bind_int(stmt, 1, count);
    vector<int> res;
    while ((rc = sqlite3_step(stmt)) == SQLITE_ROW) {
        res.push_back(sqlite3_column_int(stmt, 0));
    }

    if (rc != SQLITE_DONE) {
        fmt::print("Could not execute statement: {}", sqlite3_errmsg((sqlite3*)m_db));
        assert(false);
    }

    sqlite3_finalize(stmt);
    return res;
}

Persistence::Stats Persistence::get_stats()
{
    assert(m_db);
    const char* sql
        = "SELECT COUNT(*), SUM(won), AVG(won), AVG(duration), SUM(duration) FROM games;";
    sqlite3_stmt* stmt;
    int rc = sqlite3_prepare_v2((sqlite3*)m_db, sql, -1, &stmt, nullptr);
    if (rc != SQLITE_OK) {
        fmt::print("Could not prepare statement: {}", sqlite3_errmsg((sqlite3*)m_db));
        assert(false);
    }

    Stats res;
    rc = sqlite3_step(stmt);
    if (rc == SQLITE_ROW) {
        res.games_played = sqlite3_column_int(stmt, 0);
        res.games_won = sqlite3_column_int(stmt, 1);
        res.win_ratio = sqlite3_column_double(stmt, 2);
        res.avg_winning_time = sqlite3_column_double(stmt, 3);
        res.seconds_played = sqlite3_column_int(stmt, 4);
    } else {
        fmt::print("Could not execute statement: {}", sqlite3_errmsg((sqlite3*)m_db));
        assert(false);
    }

    sqlite3_finalize(stmt);
    return res;
}

ErrorOr<void> Persistence::save_setting(string const& key, int value)
{
    assert(m_db);
    const char* sql = "INSERT INTO settings (key, int_value) VALUES (?, ?) ON CONFLICT(key) DO "
                      "UPDATE SET int_value = excluded.int_value;";
    sqlite3_stmt* stmt;
    int rc = sqlite3_prepare_v2((sqlite3*)m_db, sql, -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
        return Error::with_message(
            "Could not prepare statement: {}", sqlite3_errmsg((sqlite3*)m_db));

    sqlite3_bind_text(stmt, 1, key.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_int(stmt, 2, value);
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE)
        return Error::with_message(
            "Could not execute statement: {}", sqlite3_errmsg((sqlite3*)m_db));

    sqlite3_finalize(stmt);
    return {};
}

ErrorOr<void> Persistence::save_setting(string const& key, float value)
{
    assert(m_db);
    const char* sql = "INSERT INTO settings (key, float_value) VALUES (?, ?) ON CONFLICT(key) DO "
                      "UPDATE SET float_value = excluded.float_value;";
    sqlite3_stmt* stmt;
    int rc = sqlite3_prepare_v2((sqlite3*)m_db, sql, -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
        return Error::with_message(
            "Could not prepare statement: {}", sqlite3_errmsg((sqlite3*)m_db));

    sqlite3_bind_text(stmt, 1, key.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_double(stmt, 2, value);
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE)
        return Error::with_message(
            "Could not execute statement: {}", sqlite3_errmsg((sqlite3*)m_db));

    sqlite3_finalize(stmt);
    return {};
}

ErrorOr<void> Persistence::save_setting(string const& key, string const& value)
{
    assert(m_db);
    const char* sql = "INSERT INTO settings (key, string_value) VALUES (?, ?) ON CONFLICT(key) DO "
                      "UPDATE SET string_value = excluded.string_value;";
    sqlite3_stmt* stmt;
    int rc = sqlite3_prepare_v2((sqlite3*)m_db, sql, -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
        return Error::with_message(
            "Could not prepare statement: {}", sqlite3_errmsg((sqlite3*)m_db));

    sqlite3_bind_text(stmt, 1, key.c_str(), -1, SQLITE_STATIC);
    sqlite3_bind_text(stmt, 2, value.c_str(), -1, SQLITE_STATIC);
    rc = sqlite3_step(stmt);
    if (rc != SQLITE_DONE)
        return Error::with_message(
            "Could not execute statement: {}", sqlite3_errmsg((sqlite3*)m_db));

    sqlite3_finalize(stmt);
    return {};
}

ErrorOr<void> Persistence::load_setting(string const& key, bool& value)
{
    int int_value;
    TRY(load_setting(key, int_value));
    value = (int_value != 0);
    return {};
}

ErrorOr<void> Persistence::load_setting(string const& key, int& value)
{
    assert(m_db);
    const char* sql = "SELECT int_value FROM settings WHERE key = ?;";
    sqlite3_stmt* stmt;
    int rc = sqlite3_prepare_v2((sqlite3*)m_db, sql, -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
        return Error::with_message(
            "Could not prepare statement: {}", sqlite3_errmsg((sqlite3*)m_db));

    sqlite3_bind_text(stmt, 1, key.c_str(), -1, SQLITE_STATIC);
    rc = sqlite3_step(stmt);
    if (rc == SQLITE_ROW) {
        value = sqlite3_column_int(stmt, 0);
    } else if (rc == SQLITE_DONE) {
        return Error::with_message("Setting not found");
    } else {
        return Error::with_message(
            "Could not execute statement: {}", sqlite3_errmsg((sqlite3*)m_db));
    }

    sqlite3_finalize(stmt);
    return {};
}

ErrorOr<void> Persistence::load_setting(string const& key, float& value)
{
    assert(m_db);
    const char* sql = "SELECT float_value FROM settings WHERE key = ?;";
    sqlite3_stmt* stmt;
    int rc = sqlite3_prepare_v2((sqlite3*)m_db, sql, -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
        return Error::with_message(
            "Could not prepare statement: {}", sqlite3_errmsg((sqlite3*)m_db));

    sqlite3_bind_text(stmt, 1, key.c_str(), -1, SQLITE_STATIC);
    rc = sqlite3_step(stmt);
    if (rc == SQLITE_ROW) {
        value = sqlite3_column_double(stmt, 0);
    } else if (rc == SQLITE_DONE) {
        return Error::with_message("Setting not found");
    } else {
        return Error::with_message(
            "Could not execute statement: {}", sqlite3_errmsg((sqlite3*)m_db));
    }

    sqlite3_finalize(stmt);
    return {};
}

ErrorOr<void> Persistence::load_setting(string const& key, string& value)
{
    assert(m_db);
    const char* sql = "SELECT string_value FROM settings WHERE key = ?;";
    sqlite3_stmt* stmt;
    int rc = sqlite3_prepare_v2((sqlite3*)m_db, sql, -1, &stmt, nullptr);
    if (rc != SQLITE_OK)
        return Error::with_message(
            "Could not prepare statement: {}", sqlite3_errmsg((sqlite3*)m_db));

    sqlite3_bind_text(stmt, 1, key.c_str(), -1, SQLITE_STATIC);
    rc = sqlite3_step(stmt);
    if (rc == SQLITE_ROW) {
        value = reinterpret_cast<const char*>(sqlite3_column_text(stmt, 0));
    } else if (rc == SQLITE_DONE) {
        return Error::with_message("Setting not found");
    } else {
        return Error::with_message(
            "Could not execute statement: {}", sqlite3_errmsg((sqlite3*)m_db));
    }

    sqlite3_finalize(stmt);
    return {};
}
