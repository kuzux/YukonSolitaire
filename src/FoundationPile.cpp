#include "FoundationPile.h"
#include <assert.h>

void FoundationPile::add_card(Card& card)
{
    assert(card.revealed());
    assert(card.suit() == m_suit);

    int topmost_value = 0;
    if (!m_cards.empty())
        topmost_value = m_cards.back()->rank().value();
    assert(card.rank().value() == topmost_value + 1);

    m_cards.push_back(&card);
    card.pile(*this);
}

void FoundationPile::lay_out_cards()
{
    for (size_t i = 0; i < m_cards.size(); i++) {
        m_cards[i]->move_to(m_position);
        m_cards[i]->move_to_layer(i + 1);
    }
}

bool FoundationPile::can_add_group_of_cards(GroupOfCards const& group) const
{
    if (group.length() != 1)
        return false;
    Card const& first_card = group.first_card();
    if (first_card.suit() != m_suit)
        return false;
    if (m_cards.empty())
        return first_card.rank() == Card::Rank::Ace;
    Card const& topmost_card = *m_cards.back();
    return first_card.rank().value() == topmost_card.rank().value() + 1;
}

void FoundationPile::add_group_of_cards(GroupOfCards const& group)
{
    assert(can_add_group_of_cards(group));
    add_card(group.first_card());
}

void FoundationPile::remove_group_of_cards(GroupOfCards const& group)
{
    assert(group.length() == 1);
    assert(m_cards.back() == &group.first_card());
    m_cards.pop_back();
}

GroupOfCards FoundationPile::cards_starting_from(Card const& starting_card)
{
    assert(!m_cards.empty());
    auto it = std::find(m_cards.begin(), m_cards.end(), &starting_card);
    assert(it == m_cards.end() - 1);

    size_t offset = it - m_cards.begin();
    size_t length = m_cards.size() - offset;
    return { *this, offset, length };
}
