#pragma once

#include <deque>
#include <functional>
#include <string>
#include <vector>

#include "Error.h"

void _cli_arg_parse(char const* arg, uint64_t* value);
void _cli_arg_parse(char const* arg, int* value);
void _cli_arg_parse(char const* arg, std::string* value);

class CliArgument
{
    friend class CliArgs;

public:
    CliArgument(bool is_flag)
        : m_is_flag(is_flag)
    {
    }
    ~CliArgument() = default;

    template <typename T> T value(std::function<T(void)> factory) const
    {
        if (!m_raw_arg)
            return factory();

        T value;
        _cli_arg_parse(m_raw_arg, &value);
        return value;
    }

    template <typename T> T value() const
    {
        assert(m_raw_arg);
        T value;
        _cli_arg_parse(m_raw_arg, &value);
        return value;
    }

    bool has_value() const { return m_raw_arg != nullptr; }

    void add_alias(std::string const& alias);
    void description(std::string const& description) { m_description = description; }
    void mark_as_required() { m_required = true; }

    void print_usage();

private:
    std::vector<std::string> m_long_aliases;
    std::vector<char> m_short_aliases;

    char* m_raw_arg { nullptr };
    std::string m_description;

    bool m_required { false };
    bool m_is_flag { false };
};

template <> inline bool CliArgument::value<bool>() const
{
    assert(m_is_flag);
    return has_value();
}

class CliArgs
{
public:
    CliArgs(int argc, char** argv)
        : m_argc(argc)
        , m_argv(argv)
    {
    }
    ~CliArgs() = default;

    CliArgument& add_argument();
    CliArgument& add_flag();
    void description(std::string const& description) { m_description = description; }

    ErrorOr<void> parse();
    void print_usage();

private:
    int m_argc;
    char** m_argv;

    std::string m_program_name;
    std::string m_description;
    std::deque<CliArgument> m_arguments;
};
