#include "GLTexture.h"
#include "Defer.h"
#include "Error.h"
#include "GLFramework.h"

#include "vendor/stb_image.h"

using namespace std;

GLTexture::Binding GLTexture::bind(GL::enum_ texture_unit) { return Binding(*this, texture_unit); }

GLTexture::Binding::Binding(GLTexture& tex, GL::enum_ texture_unit)
    : m_tex(tex)
    , m_texture_unit(texture_unit)
{
    glActiveTexture(texture_unit);
    glGetIntegerv(GL_TEXTURE_BINDING_2D, (GLint*)&m_old_glid);
    glBindTexture(GL_TEXTURE_2D, m_tex.m_glid);
    m_tex.m_is_bound = true;
}

GLTexture::Binding::~Binding()
{
    assert(m_tex.m_is_bound);
    glActiveTexture(m_texture_unit);
    glBindTexture(GL_TEXTURE_2D, m_old_glid);
    m_tex.m_is_bound = false;
}

GLTexture::GLTexture(glm::ivec2 size)
    : m_size(size)
{
    glGenTextures(1, &m_glid);
}

GLTexture::~GLTexture()
{
    if (m_glid)
        glDeleteTextures(1, &m_glid);
}

void GLTexture::set_data(const void* data, GL::enum_ format, GL::enum_ type)
{
    assert(m_is_bound);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_size.x, m_size.y, 0, format, type, data);
    glGenerateMipmap(GL_TEXTURE_2D);
}

ErrorOr<void> GLTexture::load_from_file(std::string const& filename)
{
    int width, height, channels;
    // stbi_set_flip_vertically_on_load(true);
    unsigned char* data = stbi_load(filename.c_str(), &width, &height, &channels, 4);
    if (!data)
        return Error::with_message("Failed to load image"sv);
    Defer free_data = [&] { stbi_image_free(data); };

    if (width != m_size.x || height != m_size.y)
        return Error::with_message("Image size does not match texture size"sv);
    fmt::print("Loaded image with size {}x{} and {} channels\n", width, height, channels);
    set_data(data, GL_RGBA, GL_UNSIGNED_BYTE);
    return {};
}