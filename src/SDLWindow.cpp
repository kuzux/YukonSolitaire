#include "SDLWindow.h"
#include "vendor/stb_image.h"
#include <fmt/core.h>

#include <atomic>

#ifdef USE_IMGUI
#define _STR(x) #x
#define STR(x) _STR(x)

#include STR(IMGUI_HEADER(imgui_impl_opengl3.h))
#include STR(IMGUI_HEADER(imgui_impl_sdl2.h))

#undef _STR
#undef STR

static std::atomic<size_t> s_imgui_init_count = 0;
#endif

using namespace std;

#define SDL_MUST(expr)                                                                             \
    do {                                                                                           \
        int rc = (expr);                                                                           \
        if (rc != 0) {                                                                             \
            fmt::print(stderr, "{} returned {}: {}\n", #expr, rc, SDL_GetError());                 \
            exit(1);                                                                               \
        }                                                                                          \
    } while (0)

SDLWindow::SDLWindow(string_view title, glm::ivec2 size)
    : m_window_size(size)
{
    if (!s_the) {
        s_the = this;
        m_primary = true;
    }

    uint32_t flags = SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL | SDL_WINDOW_ALLOW_HIGHDPI;
    if (m_primary)
        flags |= SDL_WINDOW_RESIZABLE;
    string title_copy(title);
    window = SDL_CreateWindow(title_copy.c_str(), 0, 0, size.x, size.y, flags);
    assert(window);

    s_windows[id()] = this;

    // NOTE: even though the gl ctx is associated with a single window, we can just
    //       create one ctx for the primary window and use it for secondary windows
    //       as well
    if (m_primary)
        ctx = SDL_GL_CreateContext(window);
    else
        ctx = SDLWindow::the().ctx;
    SDL_MUST(SDL_GL_MakeCurrent(window, ctx));

    // Only enable vsync if this is the primary window
    if (m_primary)
        SDL_MUST(SDL_GL_SetSwapInterval(1));
    else
        SDL_MUST(SDL_GL_SetSwapInterval(0));
}

SDLWindow::~SDLWindow()
{
#ifdef USE_IMGUI
    if (m_using_imgui) {
        if (--s_imgui_init_count == 0) {
            ImGui_ImplOpenGL3_Shutdown();
            ImGui_ImplSDL2_Shutdown();
            ImGui::DestroyContext();
        }
    }
#endif
    if (m_window_icon)
        SDL_FreeSurface(m_window_icon);
    if (m_primary)
        SDL_GL_DeleteContext(ctx);
    SDL_DestroyWindow(window);
    SDL_Quit();
}

void SDLWindow::draw()
{
    assert(window);

    SDL_GL_SwapWindow(window);
    m_vsynced = true;
}

void SDLWindow::set_title(string_view title)
{
    assert(window);

    string title_copy(title);
    SDL_SetWindowTitle(window, title_copy.c_str());
}

SDLEventLoop::SDLEventLoop()
{
    assert(!s_the && "Multiple SDLEventLoops are not supported");
    s_the = this;
    SDL_MUST(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO));

    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3));
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3));
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, SDL_GL_CONTEXT_PROFILE_CORE));

    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, 4));
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_DOUBLEBUFFER, 1));
    SDL_MUST(SDL_GL_SetAttribute(SDL_GL_DEPTH_SIZE, 24));
}

SDLEventLoop::~SDLEventLoop() { SDL_Quit(); }

void SDLEventLoop::run(function<void(SDLFrameInfo const&)> body)
{
    m_running = true;
    size_t frame_no = 0;
    uint64_t prev_ticks = SDL_GetTicks64();

    auto& primary = SDLWindow::the();
    while (m_running) {
        uint64_t ticks = SDL_GetTicks64();
        uint64_t delta_ticks = ticks - prev_ticks;

        // TODO: Report these performance statistics in a nicer way (optionally, obviously)
        // fmt::print("frame {}\n", frame++);
        // fmt::print("ms/frame {}\n", (float)ticks / (float)frame);
        // fmt::print("ms since last frame {}\n", ticks - prev_ticks);
        prev_ticks = ticks;
        primary.m_vsynced = false;

        while (!m_timeouts.empty() && m_timeouts.front().should_call(ticks)) {
            auto timeout = m_timeouts.front();
            m_timeouts.pop();
            if (timeout.call(ticks) == Timeout::Result::Continue)
                m_timeouts.push(timeout);
        }

        SDL_Event evt;
        while (SDL_PollEvent(&evt)) {
#ifdef USE_IMGUI
            if (primary.m_using_imgui)
                ImGui_ImplSDL2_ProcessEvent(&evt);
#endif

            emit(SDLEvent { std::move(evt) });

            if (evt.type == SDL_QUIT) {
                fmt::print("quitting\n");
                primary.audio_player().stop_all();
                m_running = false;
                break;
            }

            if (evt.type == SDL_WINDOWEVENT) {
                // window event regarding the primary window
                if (evt.window.event == SDL_WINDOWEVENT_RESIZED
                    && evt.window.windowID == primary.id())
                    primary.m_window_size = { evt.window.data1, evt.window.data2 };

                if (evt.window.event == SDL_WINDOWEVENT_CLOSE
                    && evt.window.windowID == primary.id())
                    m_running = false;
                else if (evt.window.event == SDL_WINDOWEVENT_CLOSE)
                    SDLWindow::get(evt.window.windowID).close();
            }
        }
        if (!m_running)
            break;

        body(SDLFrameInfo { ++frame_no, ticks, delta_ticks });
        // If we haven't drawn anything since we didn't need to, this means we haven't waited until
        // the V-Sync Which means we have to do the waiting ourselves (to limit our cpu usage). To
        // do that, anything close to but below 60fps is OK, so we just wait 15ms
        if (!primary.m_vsynced)
            SDL_Delay(15);
    }
}

void SDLEventLoop::stop() { m_running = false; }

void SDLWindow::set_icon(string const& filename)
{
    assert(!m_window_icon);

    int w, h, c;
    void* data = stbi_load(filename.c_str(), &w, &h, &c, 4);
    assert(c == 4);

    m_window_icon = SDL_CreateRGBSurfaceWithFormat(0, w, h, c * 8, SDL_PIXELFORMAT_ABGR8888);
    assert(m_window_icon);

    SDL_LockSurface(m_window_icon);
    memcpy(m_window_icon->pixels, data, w * h * 4);
    SDL_UnlockSurface(m_window_icon);

    SDL_SetWindowIcon(window, m_window_icon);
    stbi_image_free(data);
}

ErrorOr<void> SDLWindow::setup_imgui()
{
#ifndef USE_IMGUI
    return Error::with_message("imgui support not compiled in");
#else
    m_using_imgui = true;
    if (s_imgui_init_count++ > 0)
        return {};

    IMGUI_CHECKVERSION();

    // TODO: Create an imgui ctx here, and make it current in the make_current function
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    (void)io;

    ImGui::StyleColorsDark();

    ImGui_ImplSDL2_InitForOpenGL(window, ctx);
    ImGui_ImplOpenGL3_Init("#version 330");

    return {};
#endif
}

AudioPlayer& SDLWindow::audio_player()
{
    if (!m_audio_player) {
        m_audio_player = new AudioPlayer();
        MUST(m_audio_player->initialize());
    }
    return *m_audio_player;
}

void SDLWindow::fullscreen(bool value)
{
    if (value)
        SDL_MUST(SDL_SetWindowFullscreen(window, SDL_WINDOW_FULLSCREEN_DESKTOP));
    else
        SDL_MUST(SDL_SetWindowFullscreen(window, 0));
}

Timeout::Timeout(uint64_t interval, Callback callback)
    : m_interval(interval)
    , m_callback(callback)
{
    m_last_call = 0;
}

Timeout::~Timeout() { }

bool Timeout::should_call(uint64_t ticks) const { return ticks - m_last_call >= m_interval; }

Timeout::Result Timeout::call(uint64_t ticks)
{
    m_last_call = ticks;
    return m_callback(ticks);
}

void SDLEventLoop::in(uint64_t interval, Timeout::Callback callback)
{
    m_timeouts.push(Timeout(interval, callback));
}

glm::vec2 SDLWindow::gl_coordinates(glm::ivec2 sdl_coordinates) const
{
    auto width = m_window_size.x;
    auto height = m_window_size.y;
    float mouse_x = ((float)sdl_coordinates.x / width) * 2.f - 1.f;
    float mouse_y = ((float)sdl_coordinates.y / height) * 2.f - 1.f;
    mouse_y = -mouse_y;

    return { mouse_x, mouse_y };
}
