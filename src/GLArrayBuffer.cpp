#include "GLArrayBuffer.h"
#include "GLFramework.h"

static GL::enum_ _gl_target(GLBufferTarget target)
{
    switch (target) {
    case GLBufferTarget::ArrayBuffer:
        return GL_ARRAY_BUFFER;
    case GLBufferTarget::ElementArrayBuffer:
        return GL_ELEMENT_ARRAY_BUFFER;
    }

    __builtin_unreachable();
}

static GL::enum_ _gl_target_binding(GLBufferTarget target)
{
    switch (target) {
    case GLBufferTarget::ArrayBuffer:
        return GL_ARRAY_BUFFER_BINDING;
    case GLBufferTarget::ElementArrayBuffer:
        return GL_ELEMENT_ARRAY_BUFFER_BINDING;
    }

    __builtin_unreachable();
}

ErrorOr<void> _gl_buffer_data(GLBufferTarget target, size_t bytes, void const* data)
{
    glBufferData(_gl_target(target), bytes, data, GL_STATIC_DRAW);
    if (auto err = glGetError())
        return Error::with_message(fmt::format("glBufferData failed with error {}", err));
    return {};
}

GL::uint _gl_bind_buffer(GL::uint glid, GLBufferTarget target)
{
    // Make sure no other buffer is bound
    GLint currently_bound_buffer;
    glGetIntegerv(_gl_target_binding(target), &currently_bound_buffer);
    assert(currently_bound_buffer >= 0);

    if (currently_bound_buffer != (GLint)glid)
        glBindBuffer(_gl_target(target), glid);
    return currently_bound_buffer;
}

GL::uint _gl_arraybuffer_create()
{
    GL::uint glid;
    glGenBuffers(1, &glid);
    return glid;
}

void _gl_arraybuffer_cleanup(GL::uint glid) { glDeleteBuffers(1, &glid); }