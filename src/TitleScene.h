#pragma once

#include <memory>

#include "Scene.h"

class TitleScene : public Scene
{
    struct Impl;

public:
    TitleScene();
    ~TitleScene();

    void attach_to(SDLEventLoop& event_loop) override;
    void detach() override;

    void draw(SDLFrameInfo const& frame) override;
    void draw_ui() override;

private:
    void draw_high_scores();
    void draw_main_menu();
    void draw_stats();
    void draw_seed_input();
    void draw_settings();

    void load_settings();
    void save_settings();

    Impl& impl() { return *m_impl; }

    std::unique_ptr<Impl> m_impl;
    bool m_show_high_scores { false };
    bool m_show_stats { false };
    bool m_show_seed_input { false };
    bool m_show_settings { false };
};
