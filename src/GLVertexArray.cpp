#include "GLVertexArray.h"

#include "GLFramework.h"
#include <cstring>

using namespace std;

GLVertexArray::GLVertexArray() { glGenVertexArrays(1, &m_glid); }

GLVertexArray::GLVertexArray(GLVertexArray&& o)
    : m_glid(o.m_glid)
    , m_enabled_attrs(std::move(o.m_enabled_attrs))
{
    assert(!o.m_is_bound);
    o.m_glid = 0;
}

GLVertexArray::~GLVertexArray()
{
    assert(!m_is_bound);

    if (m_glid) {
        glDeleteVertexArrays(1, &m_glid);
    }
}

GLVertexArray::Binding::Binding(GLVertexArray& vao)
    : m_vao(vao)
{
    assert(!m_vao.m_is_bound);
    m_vao.m_is_bound = true;

    GLint currently_bound_vao;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &currently_bound_vao);
    assert(currently_bound_vao >= 0);
    m_old_glid = currently_bound_vao;

    if (m_vao.m_glid != m_old_glid)
        glBindVertexArray(m_vao.m_glid);
}

GLVertexArray::Binding::~Binding()
{
    assert(m_vao.m_is_bound);
    GLint currently_bound_vao;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &currently_bound_vao);
    assert(currently_bound_vao == (GLint)m_vao.m_glid);

    if (m_vao.m_glid != m_old_glid)
        glBindVertexArray(m_old_glid);
    m_vao.m_is_bound = false;
}

GLVertexArray::Binding GLVertexArray::bind() { return Binding(*this); }

void GLVertexArray::define_attribute(uint32_t index, Attribute attr)
{
    assert(m_is_bound);
    bool attr_exists = m_enabled_attrs.find(index) != m_enabled_attrs.end();
    assert(!attr_exists && "Trying to enable attr with same index");

    GLint currently_bound_vao;
    glGetIntegerv(GL_VERTEX_ARRAY_BINDING, &currently_bound_vao);
    assert(currently_bound_vao == (GLint)m_glid
        && "Trying to define an attribute but the vao is not bound");

    m_enabled_attrs.insert(index);
    glEnableVertexAttribArray(index);
    if (attr.gl_type == GL_INT)
        glVertexAttribIPointer(
            index, attr.num_components, attr.gl_type, attr.stride, (void*)attr.offset);
    else
        glVertexAttribPointer(
            index, attr.num_components, attr.gl_type, GL_FALSE, attr.stride, (void*)attr.offset);

    if (attr.divisor)
        glVertexAttribDivisor(index, attr.divisor);
}

int32_t __num_components_gl_attribute(const char* class_name)
{
    if (!strcmp(class_name, "float"))
        return 1;
    if (!strcmp(class_name, "glm::vec2"))
        return 2;
    if (!strcmp(class_name, "glm::vec3"))
        return 3;
    if (!strcmp(class_name, "glm::vec4"))
        return 4;

    if (!strcmp(class_name, "int"))
        return 1;
    if (!strcmp(class_name, "glm::ivec2"))
        return 2;
    if (!strcmp(class_name, "glm::ivec3"))
        return 3;
    if (!strcmp(class_name, "glm::ivec4"))
        return 4;

    assert(false && "Unknown gl attribute type");
}

GL::enum_ __gl_type_gl_attribute(const char* class_name)
{
    if (!strcmp(class_name, "float"))
        return GL_FLOAT;
    if (!strcmp(class_name, "glm::vec2"))
        return GL_FLOAT;
    if (!strcmp(class_name, "glm::vec3"))
        return GL_FLOAT;
    if (!strcmp(class_name, "glm::vec4"))
        return GL_FLOAT;

    if (!strcmp(class_name, "int"))
        return GL_INT;
    if (!strcmp(class_name, "glm::ivec2"))
        return GL_INT;
    if (!strcmp(class_name, "glm::ivec3"))
        return GL_INT;
    if (!strcmp(class_name, "glm::ivec4"))
        return GL_INT;

    assert(false && "Unknown gl attribute type");
}
