#pragma once

#include <glm/glm.hpp>
#include <memory>

#include "CardGame.h"
#include "Scene.h"

class GameScene : public Scene
{
    struct Impl;

public:
    GameScene();
    ~GameScene();

    void attach_to(SDLEventLoop& event_loop) override;
    void detach() override;

    void draw(SDLFrameInfo const& frame) override;
    void draw_ui() override;

    void draw_secondary(SDLFrameInfo const& frame) override;

private:
    Impl& impl() { return *m_impl; }

    glm::vec2 default_card_scale() const;
    void resize_card_sprites();
    void update_aspect_ratio(float aspect_ratio);

    CardGame m_game;
    std::unique_ptr<Impl> m_impl;
};
