#pragma once

#include "Card.h"
#include "GroupOfCards.h"
#include <glm/glm.hpp>
#include <vector>

class AbstractPile
{
public:
    virtual glm::vec2 position() const = 0;

    virtual bool can_add_group_of_cards(GroupOfCards const& group) const = 0;
    virtual void add_group_of_cards(GroupOfCards const& group) = 0;
    virtual void remove_group_of_cards(GroupOfCards const& group) = 0;
    virtual GroupOfCards cards_starting_from(Card const& starting_card) = 0;

    // TODO: Get a better way to access a card in a group of cards
    virtual std::vector<Card*>& cards() = 0;

    virtual void lay_out_cards() = 0;
    virtual ~AbstractPile() = default;
};
