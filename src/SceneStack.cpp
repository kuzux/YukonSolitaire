#include "SceneStack.h"

#include "SDLWindow.h"

SceneStack::SceneStack()
{
    assert(s_the == nullptr);
    s_the = this;
}

void SceneStack::pop()
{
    auto& event_loop = SDLEventLoop::the();
    m_scenes.back()->detach();
    m_scenes.pop_back();
    if (!m_scenes.empty())
        m_scenes.back()->attach_to(event_loop);
}

void SceneStack::attach_new_scene()
{
    auto& event_loop = SDLEventLoop::the();
    if (m_scenes.size() > 1) {
        auto& old_scene = *m_scenes[m_scenes.size() - 2];
        old_scene.detach();
    }
    m_scenes.back()->attach_to(event_loop);
}
