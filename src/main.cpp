#include "AudioPlayer.h"
#include "CliArgs.h"
#include "GLEWLoader.h"
#include "GLFramework.h"
#include "GameScene.h"
#include "Persistence.h"
#include "Random.h"
#include "SDLWindow.h"
#include "SceneStack.h"
#include "TitleScene.h"

#include "platform/platform.h"

#include "vendor/imgui.h"
#include "vendor/imgui_impl_opengl3.h"
#include "vendor/imgui_impl_sdl2.h"

#include <fmt/core.h>

bool g_has_secondary = true; // to be accessed elsewhere

extern "C" const char* __asan_default_options() { return "detect_leaks=0"; }

int main(int argc, char** argv)
{
    CliArgs cli_args(argc, argv);
    cli_args.description("Yukon Solitaire Game");

    auto& random_seed_arg = cli_args.add_argument();
    random_seed_arg.description("Set the random seed");
    random_seed_arg.add_alias("--seed");
    random_seed_arg.add_alias("-s");

    auto& no_title_arg = cli_args.add_flag();
    no_title_arg.description("Disable the title screen");
    no_title_arg.add_alias("--no-title");
    no_title_arg.add_alias("-n");

    auto& secondary_arg = cli_args.add_flag();
    secondary_arg.description("Enable secondary (debug) window");
    secondary_arg.add_alias("--secondary");

    auto maybe_parse_error = cli_args.parse();
    if (maybe_parse_error.is_error()) {
        cli_args.print_usage();
        return 1;
    }

    auto random_seed = random_seed_arg.value<uint64_t>(Random::time_seed);
    bool no_title = no_title_arg.value<bool>();
    g_has_secondary = secondary_arg.value<bool>();

    Random::seed(random_seed);
    fmt::print("Using Random seed: {}\n", random_seed);

    SDLEventLoop event_loop;
    SDLWindow window("Yukon Solitaire", { 1024, 768 });

    std::optional<SDLWindow> secondary_window;
    if (g_has_secondary)
        // NOTE: I'm not sure why I have to explicitly specify the type ivec2 here
        secondary_window.emplace("Secondary Window", glm::ivec2 { 800, 600 });

    MUST(window.setup_imgui());
    window.set_icon(get_resource_path("icon.png"));

    GLEWLoader gl_extensions = MUST(GLEWLoader::create());
#ifndef __APPLE__
    gl_extensions.require("GL_ARB_draw_instanced");
#endif

    MUST(Persistence::the().load_database(get_appdata_path("games.db")));
    glEnable(GL_BLEND);
    glEnable(GL_DEPTH_TEST);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glClearColor(0.1f, 0.5f, 0.1f, 1.0f);

    auto& audio_player = window.audio_player();
    auto bg_music = MUST(Sound::load(get_resource_path("chill_loopable.opus"), Sound::Type::Music));
    audio_player.play_looping(bg_music);

    SceneStack scene_stack;
    if (!no_title)
        scene_stack.push<TitleScene>();
    else
        scene_stack.push<GameScene>();

    event_loop.run([&](SDLFrameInfo const& frame) {
        window.make_current();

        auto& scene = scene_stack.current_scene();
        scene.draw(frame);

        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplSDL2_NewFrame();
        ImGui::NewFrame();
        scene.draw_ui();
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        window.draw();

        if (g_has_secondary) {
            secondary_window->make_current();
            scene.draw_secondary(frame);
            secondary_window->draw();
        }
    });

    return 0;
}
