#pragma once

#include <assert.h>
#include <fmt/core.h>
#include <optional>
#include <string>
#include <string_view>

class Void
{
};

class Error
{
public:
    inline static Error with_message(std::string&& message) { return Error { std::move(message) }; }

    template <typename... Args>
    inline static Error with_message(std::string_view fmt, Args&&... args)
    {
        return Error { fmt::format(fmt::runtime(fmt), std::forward<Args>(args)...) };
    }

    std::string_view message() const { return m_message; }

private:
    Error(std::string const& message)
        : m_message(message)
    {
    }
    Error(std::string&& message)
        : m_message(message)
    {
    }
    std::string m_message;
};

template <typename T> class ErrorOr
{
public:
    ErrorOr(T t)
        : m_value(std::move(t))
        , m_error({})
    {
    }
    ErrorOr(Error err)
        : m_value({})
        , m_error(err)
    {
    }

    bool has_value() const { return m_value.has_value(); }
    bool is_error() const { return m_error.has_value(); }
    T const& value() const
    {
        assert(has_value());
        return m_value.value();
    }
    T release_value()
    {
        assert(has_value());
        return std::move(*m_value);
    }
    Error const& error()
    {
        assert(!has_value());
        return m_error.value();
    }

private:
    std::optional<T> m_value;
    std::optional<Error> m_error;
};

template <> class ErrorOr<void> : public ErrorOr<Void>
{
public:
    ErrorOr()
        : ErrorOr<Void>(Void {})
    {
    }
    ErrorOr(Error err)
        : ErrorOr<Void>(err)
    {
    }
};

template <> struct fmt::formatter<Error> {
    template <typename ParseContext> constexpr auto parse(ParseContext& ctx) { return ctx.begin(); }

    template <typename FormatContext> auto format(const Error& err, FormatContext& ctx) const
    {
        return format_to(ctx.out(), "Error: {}", err.message());
    }
};

#define MUST(exp)                                                                                  \
    ({                                                                                             \
        auto&& __must__res = (exp);                                                                \
        if (__must__res.is_error()) {                                                              \
            fmt::print(stderr, "ERROR: {}\n", __must__res.error());                                \
            assert(false);                                                                         \
        }                                                                                          \
        __must__res.release_value();                                                               \
    })

#define TRY(exp)                                                                                   \
    ({                                                                                             \
        auto&& __try__res = (exp);                                                                 \
        if (__try__res.is_error()) {                                                               \
            return __try__res.error();                                                             \
        }                                                                                          \
        __try__res.release_value();                                                                \
    })
