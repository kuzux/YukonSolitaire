#pragma once

// make osx complain less about deprecated stuff
#define GL_SILENCE_DEPRECATION 1

#include <GL/glew.h>

#define GL_GLEXT_PROTOTYPES 1
#ifdef __APPLE__
#include <OpenGL/gl.h>
#else
#include <GL/gl.h>
#endif
